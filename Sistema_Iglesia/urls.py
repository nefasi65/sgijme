"""Sistema_Iglesia URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import  import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path, re_path, reverse_lazy
from django.contrib import admin
admin.site.site_url = '/portada/index'
from django.contrib.auth import views
from apps.persona import views
from apps.ministerio import views
from apps.usuario import views
from apps.evento import views
from apps.movimiento_dinero import views
from apps.celula import views
from apps.discipulado import views
from django.contrib.auth.views import LoginView, LogoutView, PasswordResetDoneView, PasswordResetView, PasswordResetCompleteView, PasswordResetConfirmView, PasswordChangeView, PasswordChangeDoneView, PasswordResetDoneView #login, logout_then_login, password_reset, password_reset_done, password_reset_confirm, password_reset_complete
from apps.usuario.views import RegistroUsuario
from apps.portada import views
from django.conf.urls.static import static
from django.conf import settings

app_name = 'authapp'

urlpatterns = [
    #url(r'^admin/', admin.site.urls, name='administrar'),
    re_path(r'^admin/', admin.site.urls, name='administrar'),
    re_path(r'^persona/', include(('apps.persona.urls', 'persona'), namespace='persona')),
    #url(r'^matrimonio/', include('apps.persona.urls', namespace='matrimonio')),
    re_path(r'^movimiento/', include(('apps.movimiento_dinero.urls', 'movimiento_dinero'), namespace='movimiento')),
    #url(r'^categoria/', include('apps.movimiento_dinero.urls', namespace='categoria')),
    re_path(r'^ministerio/', include(('apps.ministerio.urls', 'ministerio'), namespace='ministerio')),
    re_path(r'^evento/', include(('apps.evento.urls', 'evento'), namespace='evento')),
    re_path(r'^celula/', include(('apps.celula.urls', 'celula'), namespace='celula')),
    re_path(r'^articulo/', include(('apps.inventario.urls', 'inventario'), namespace='articulo')),
    #url(r'^unidad_de_medida/', include('apps.inventario.urls', namespace='unidad_de_medida')),
    re_path(r'^entrada/', include(('apps.entrada.urls', 'entrada'), namespace='entrada')),
    re_path(r'^salida/', include(('apps.salida.urls', 'salida'), namespace='salida')),
    re_path(r'^entradasalida/', include(('apps.entradasalida.urls', 'entradasalida'), namespace='entradasalida')),
    re_path(r'^portada/', include(('apps.portada.urls', 'portada'), namespace='portada')),
    re_path(r'^discipulado/', include(('apps.discipulado.urls', 'discipulado'), namespace='discipulado')),
    re_path(r'^portada/', include(('apps.portada.urls','portada'), namespace='portada')),
    re_path(r'^usuario/', include(('apps.usuario.urls', 'usuario'), namespace='usuario')),
    re_path(r'^ckeditor/', include('ckeditor_uploader.urls')),
#    url(r'^accounts/login/', login, {'template_name':'index.html'}, name='login'), -----Django 1.8
#    url(r'^logout/', logout_then_login, name='logout'),
    #re_path(r'^accounts/login/$', LoginView.as_view(template_name='index.html'), name='login'), #-----Django 3.1.4
    #re_path(r'^logout/$', LogoutView.as_view(), name='logout'), #-----Django 3.1.4

    re_path(r'^accounts/login/$', LoginView.as_view(template_name='index.html'), name='login'),
    re_path(r'^accounts/logout/$', LogoutView.as_view(template_name='portada/post_list.html'), name='logout'),
    re_path(r'^password_change/', PasswordChangeView.as_view(
        template_name='registration/password_change_form.html'), name='password_change'),
    re_path(r'^password_change/done/', PasswordChangeDoneView.as_view(template_name='registration/password_change_done.html'),
         name='password_change_done'),
    re_path(r'^reset/password_reset', PasswordResetView.as_view(
        template_name='registration/password_reset_form.html',
        email_template_name='registration/password_reset_email.html'), name='password_reset'),
    re_path(r'^password_reset_done', PasswordResetDoneView.as_view(
        template_name='registration/password_reset_done.html'), name='password_reset_done'),

    re_path(r'^reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', PasswordResetConfirmView.as_view(
        template_name='registration/password_reset_confirm.html',), name='password_reset_confirm'),

    re_path(r'^reset/done', PasswordResetCompleteView.as_view(
        template_name='registration/password_reset_complete.html'), name='password_reset_complete'),




    #url(r'^password/recovery/',
	#	RegistroUsuario.as_view(
	#		template_name='restration/password_reset_form.html',
	#		email_template_name='restration/password_reset_email.html',
	#	),
	#	name='password_reset',
	#	),
#    re_path(r'^reset/password_reset', PasswordResetView.as_view(),
#        {'template_name':'registration/password_reset_form.html',
#        'email_template_name': 'registration/password_reset_email.html'},
#        name='password_reset'),

#    re_path(r'^password_reset_done', PasswordResetDoneView.as_view(),
#        {'template_name': 'registration/password_reset_done.html'},
#        name='password_reset_done'),

#    re_path(r'^password/recovery/(?P<uidb64>[0-9A-Za-z_\-]+)/'
#        r'(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', PasswordResetConfirmView.as_view(),
#        {'template_name': 'registration/password_reset_confirm.html'},
#        name='password_reset_confirm'),

    # url(
    #    r'^password/recovery/(?P<uidb64>[0-9A-Za-z_\-]+)/'
    #    r'(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
    #    auth_views.PasswordResetConfirmView.as_view(
    #        success_url=reverse_lazy('home'),
    #        post_reset_login=True,
    #        template_name='registration/password_reset_confirm.html',
    #        post_reset_login_backend=(
    #            'django.contrib.auth.backends.AllowAllUsersModelBackend'
    #        ),
    #    ),
    #    name='password_reset_confirm',
    #),

#    re_path(r'^reset/done', PasswordResetCompleteView, {'template_name': 'registration/password_reset_complete.html'},
#        name='password_reset_complete'),




] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
