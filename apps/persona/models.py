from django.db import models
from apps.validators import numero_validation, string_validation, celular_validation, dni_validation
from django.db.models import Q

# Create your models here.
"""class Matrimonio(models.Model):
    apellido_matrimonio = models.CharField(max_length=50)
    fecha_baja_matrimonio = models.DateField(max_length=50, blank=True, null=True)
    def __str__(self):
        return self.apellido_matrimonio
"""
#rol para asignar a celulas y ministerios.
"""
class Rol(models.Model):
    descripcion_rol = models.CharField(max_length=30)
    def __str__(self):
        return self.descripcion_rol
"""

estado_civil_choices = (
    ('Casado/a', 'Casado/a'),
    ('Soltero/a', 'Soltero/a'),
    ('Viudo/a', 'Viudo/a'),
    ('Divorciado/a', 'Divorciado/a'),
)

si_no_choices = (
    ('Sí', 'Sí'),
    ('No', 'No'),
)

si_no_enCurso_choices = (
    ('Sí', 'Sí'),
    ('No', 'No'),
    ('En curso', 'En curso'),
)
sexo_choices = (
    ('M', 'Masculino'),
    ('F', 'Femenino'),
)

class Persona(models.Model):
    nombre = models.CharField(max_length=40, blank=False, validators=[string_validation])
    apellido_persona = models.CharField(max_length=40, blank=False, validators=[string_validation])
    fecha_nacimiento = models.CharField(max_length=10, null=False, blank=False)
    dni = models.CharField(max_length=10, unique=True, blank=False, validators=[dni_validation], help_text='Separar por puntos')
    sexo = models.CharField(choices=sexo_choices, max_length=10, blank=False)
    celular = models.CharField(max_length=18, blank=True, validators=[celular_validation], default='+ 54 9 3493 ', help_text='Formato: +54 9 0123 456789')
    direccion = models.CharField(max_length=25, blank=False)
    nro_casa = models.CharField(max_length=5, blank=False, validators=[numero_validation])
    piso = models.CharField(max_length=5, blank=True, validators=[numero_validation])
    departamento = models.CharField(max_length=5, blank=True)
    #id_matrimonio = models.ForeignKey(Matrimonio, null=True, blank=True, on_delete=models.CASCADE)
    #conyugue = models.ForeignKey(Persona, null=True, blank=True, on_delete=models.CASCADE)
    localidad = models.CharField(max_length=25, validators=[string_validation], blank=False)
    estado_civil = models.CharField(max_length=12, choices=estado_civil_choices,)
    profesion = models.CharField(max_length=35, blank=True, validators=[string_validation])
    bautizado = models.CharField(max_length=2, choices=si_no_choices,)
    escuela_biblica = models.CharField(max_length=10, choices=si_no_enCurso_choices,)
    encuentro = models.CharField(max_length=2, choices=si_no_choices,)
    fecha_baja_persona = models.DateField(max_length=10, blank=True, null=True)

    def __str__(self):
        return '{} {}'.format(self.nombre,self.apellido_persona)

    class Meta:
            permissions = (('ver_detalle', 'Ver detalle persona'),)
