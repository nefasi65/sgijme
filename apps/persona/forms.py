from django import forms
from apps.persona.models import Persona#, Matrimonio



class PersonaForm(forms.ModelForm):

    class Meta:
        model = Persona

        fields = [
            'nombre',
            'apellido_persona',
            'dni',
            'fecha_nacimiento',
            'sexo',
            'celular',
            'direccion',
            'nro_casa',
            'piso',
            'departamento',
            #'id_matrimonio',
            'estado_civil',
            'profesion',
            'bautizado',
            'escuela_biblica',
            'encuentro',
            'localidad',
        ]
        labels = {
            'nombre': 'Nombre',
            'apellido_persona': 'Apellido',
            'dni':'DNI',
            'fecha_nacimiento': 'Fecha de nacimiento',
            'sexo': 'Sexo',
            'celular': 'Celular',
            'direccion': 'Calle',
            'nro_casa': 'Nro',
            'piso': 'Piso',
            'departamento': 'Dpto',
            #'id_matrimonio': 'Matrimonio',
            'estado_civil':'Estado civil',
            'profesion': 'Profesión',
            'bautizado': 'Bautizado',
            'escuela_biblica': 'Escuela bíblica',
            'encuentro': 'Encuentro',
            'localidad': 'Localidad',

        }


"""
class MatrimonioForm(forms.ModelForm):
    class Meta:
        model = Matrimonio
        fields = [
                'apellido_matrimonio'
        ]
        labels = {
                'apellido_matrimonio':'Apellido Matrimonio'
        }
"""
