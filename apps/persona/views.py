from django.shortcuts import render, redirect
from django.http import HttpResponse
from apps.persona.forms import PersonaForm#, MatrimonioForm
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, TemplateView
from django.urls import reverse_lazy
from django.urls import reverse
import datetime
from django.db.models import Q
from django.contrib.auth.decorators import login_required

from apps.persona.models import Persona#, Matrimonio

# Persona
@login_required
def persona_view(request):
    if request.method == 'POST':
        form = PersonaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('persona:persona_listar')
    else:
        form = PersonaForm
    return render(request, 'persona/persona_form.html', {'form':form})

@login_required
def persona_list(request):
	persona = Persona.objects.filter(fecha_baja_persona__isnull=True).order_by('apellido_persona')
	contexto = {'personas':persona}
	return render(request, 'persona/persona_list.html', contexto)


@login_required
def persona_list_baja(request):
	persona = Persona.objects.filter(fecha_baja_persona__isnull=False).order_by('apellido_persona')
	contexto = {'personas':persona}
	return render(request, 'persona/persona_list_baja.html', contexto)

@login_required
def persona_edit(request, pk):
    persona = Persona.objects.get(id=pk)
    if request.method == 'GET':
        form = PersonaForm(instance=persona)
    else:
        form = PersonaForm(request.POST, instance=persona)
        if form.is_valid():
            form.save()
            return redirect('persona:persona_listar')
    return render(request,'persona/persona_form.html',{'form':form})

@login_required
def persona_delete(request, pk):
    persona = Persona.objects.get(id=pk)
    if request.method == 'POST':
        persona.fecha_baja_persona = datetime.date.today()
        persona.save()
        return redirect('persona:persona_listar')
    return render(request,'persona/persona_delete.html',{'persona':persona})

@login_required
def persona_restore(request, pk):
    persona = Persona.objects.get(id=pk)
    if request.method == 'GET':
        persona.fecha_baja_persona = None
        persona.save()
        return redirect('persona:persona_listar_baja')
    return redirect('persona:persona_listar_baja')

@login_required
def get_queryset_persona(self, baja):
    if baja == "Si":
        queryset = Persona.objects.filter(fecha_baja_persona__isnull=False).order_by('apellido_persona')
    else:
        queryset = Persona.objects.filter(fecha_baja_persona__isnull=True).order_by('apellido_persona')
    return queryset

@login_required
def buscar_nombre_persona(request):
    query = request.GET.get('q')
    baja = request.GET.get('baja')
    if query is not None and query != ' ' and request.is_ajax():
        personas = get_queryset_persona(request, baja).filter(Q(nombre__icontains=query)|Q(apellido_persona__icontains=query))
        return render(request, 'persona/buscar_persona.html', {'personas': personas})
    personas =  get_queryset_persona(request, baja)
    return render(request, 'persona/buscar_persona.html', {'personas': personas})

@login_required
def buscar_dni_persona(request):
    query = request.GET.get('q')
    baja = request.GET.get('baja')
    if query is not None and query != ' ' and request.is_ajax():
        personas = get_queryset_persona(request,baja).filter(dni__icontains=query)
        print(personas)
        return render(request, 'persona/buscar_persona.html', {'personas': personas})
    personas =  get_queryset_persona(request, baja)
    return render(request, 'persona/buscar_persona.html', {'personas': personas})


"""
#Matrimonio
def matrimonio_view(request):
    if request.method == 'POST':
        form = MatrimonioForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('matrimonio:matrimonio_listar')
    else:
        form = MatrimonioForm
    return render(request, 'matrimonio/matrimonio_form.html', {'form':form})


def matrimonio_list(request):
	matrimonio = Matrimonio.objects.filter(fecha_baja_matrimonio__isnull=True)
	contexto = {'matrimonios':matrimonio}
	return render(request, 'matrimonio/matrimonio_list.html', contexto)

def matrimonio_list_baja(request):
	matrimonio = Matrimonio.objects.filter(fecha_baja_matrimonio__isnull=False)
	contexto = {'matrimonios':matrimonio}
	return render(request, 'matrimonio/matrimonio_list_baja.html', contexto)

def matrimonio_edit(request, pk):
    matrimonio = Matrimonio.objects.get(id=pk)
    if request.method == 'GET':
        form = MatrimonioForm(instance=matrimonio)
    else:
        form = MatrimonioForm(request.POST, instance=matrimonio)
        if form.is_valid():
            form.save()
            return redirect('matrimonio:matrimonio_listar')
    return render(request,'matrimonio/matrimonio_form.html',{'form':form})

def matrimonio_delete(request, pk):
    matrimonio = Matrimonio.objects.get(id=pk)
    if request.method == 'POST':
        matrimonio.fecha_baja_matrimonio = datetime.date.today()
        matrimonio.save()
        return redirect('matrimonio:matrimonio_listar')
    return render(request,'matrimonio/matrimonio_delete.html',{'matrimonio':matrimonio})

def matrimonio_restore(request, pk):
    matrimonio = Matrimonio.objects.get(id=pk)
    if request.method == 'GET':
        matrimonio.fecha_baja_matrimonio = None
        matrimonio.save()
        return redirect('matrimonio:matrimonio_listar')
    return redirect('matrimonio:matrimonio_listar_baja')
"""

class BuscarView(TemplateView):
    def post(self, request, *args, **kwargs):
        buscar = request.POST['buscalo']
        per1 = Persona.objects.filter(nombre__icontains=buscar)
        per2 = Persona.objects.filter(apellido_persona__icontains=buscar)
        per3 = Persona.objects.filter(fecha_nacimiento__icontains=buscar)
        per4 = Persona.objects.filter(sexo__icontains=buscar)
        per5 = Persona.objects.filter(celular__icontains=buscar)
        per6 = Persona.objects.filter(direccion__icontains=buscar)
        per7 = Persona.objects.filter(localidad__icontains=buscar)
        per8 = Persona.objects.filter(estado_civil__icontains=buscar)
        per9 = Persona.objects.filter(profesion__icontains=buscar)          ## ver como hacer la busqueda por bautizado, escuela biblica y encuentro que son valores si/no
        contexto = {
        'personas1':per1,
        'personas2':per2,
        'personas3':per3,
        'personas4':per4,
        'personas5':per5,
        'personas6':per6,
        'personas7':per7,
        'personas8':per8,
        'personas9':per9,
        }
        return render(request,'persona/buscar.html',contexto)
