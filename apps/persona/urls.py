from django.urls import include, path, re_path
from apps.persona import views
from django.contrib.auth.decorators import login_required
from apps.persona.views import persona_view, persona_list, persona_list_baja, persona_restore, persona_edit, buscar_nombre_persona, persona_delete, BuscarView, buscar_dni_persona


urlpatterns = [
    re_path(r'^nuevo$',views.persona_view, name='persona_crear'),
    re_path(r'^listar$', views.persona_list, name='persona_listar'),
    re_path(r'^listar/baja/$', views.persona_list_baja, name='persona_listar_baja'),
    re_path(r'^editar/(?P<pk>\d+)/$', views.persona_edit, name='persona_editar'),
    re_path(r'^eliminar/(?P<pk>\d+)/$', views.persona_delete, name='persona_eliminar'),
    re_path(r'^restaurar/(?P<pk>\d+)/$', views.persona_restore, name='persona_restaurar'),
    #url(r'^nuevo_matrimonio$',views.matrimonio_view, name='matrimonio_crear'),
    #url(r'^listar_matrimonio$', views.matrimonio_list, name='matrimonio_listar'),
    #url(r'^matrimonio/listar/baja/$', views.matrimonio_list_baja, name='matrimonio_listar_baja'),
    #url(r'^editar/matrimonio/(?P<pk>\d+)/$', views.matrimonio_edit, name='matrimonio_editar'),
    #url(r'^eliminar_matrimonio/(?P<pk>\d+)/$',  views.matrimonio_delete, name='matrimonio_eliminar'),
    #surl(r'^matrimonio/restaurar/(?P<pk>\d+)/$', views.matrimonio_restore, name='matrimonio_restaurar'),
    re_path(r'^buscar/$', BuscarView.as_view(),name='buscar'),
    re_path(r'^buscarnombrepersona/$', views.buscar_nombre_persona, name='buscarnombrepersona'),
    re_path(r'^listar/baja/buscarnombrepersona/$', views.buscar_nombre_persona, name='buscarnombrepersona'),
    re_path(r'^buscardnipersona/$', views.buscar_dni_persona, name='buscardnipersona'),
    re_path(r'^listar/baja/buscardnipersona/$', views.buscar_dni_persona, name='buscardnipersona'),
]
