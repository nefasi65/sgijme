from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

def numero_validation(value):
    if not value.isdigit():
        raise ValidationError(
        _('%(value)s no es válido. Por favor ingresar valores numéricos'),
        params={'value':value},
        )

letras = [' ','.','á', 'é', 'í', 'ó', 'ú', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', \
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

letras2 = ['.','á', 'é', 'í', 'ó', 'ú', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', \
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

def string_validation(value):
    #if value.isspace():
    #    raise ValidationError(
    #    _('%(value)s no es válido. Por favor ingresar valores de texto'),
    #    params={'value':value},
    #    )
    #else:
        for x in value:
            if not x in letras:
                raise ValidationError(
                _('%(value)s no es válido. Por favor ingresar valores de texto'),
                params={'value':value},
                )

def celular_validation(value):
        for x in value:
            if x in letras2 or len(value) < 17:
                raise ValidationError(
                _('%(value)s no es válido'),
                params={'value':value},
                )

dni = ['.','0','1','2','3','4','5','6','7','8','9']
def dni_validation(value):
    for x in value:
        if not x in dni or len(value) > 10:
            raise ValidationError(
            _('%(value)s no es válido. Por favor ingresar un DNI válido.'),
            params={'value':value},
            )
