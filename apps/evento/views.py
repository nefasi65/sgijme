from django.shortcuts import render, redirect

# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.urls import reverse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, TemplateView
import datetime
from apps.evento.models import Evento
from apps.evento.forms import EventoForm
from django.db.models import Q
from django.contrib.auth.decorators import login_required
#from apps.persona.models import Persona # en apps\ministerio\views tambien esta

from .models import Evento


@login_required
def evento_view(request):
    if request.method == 'POST':
        form = EventoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('evento:evento_listar')
    else:
        form = EventoForm
    return render(request, 'evento/evento_form.html', {'form':form})

@login_required
def evento_list(request):
	evento = Evento.objects.filter(fecha_baja_evento__isnull=True)
	contexto = {'eventos':evento}
	return render(request, 'evento/evento_listar.html', contexto)

@login_required
def get_queryset_evento(self):
    queryset = Evento.objects.filter(fecha_baja_evento__isnull=True).order_by('descripcion_evento')
    return queryset

@login_required
def evento_list_baja(request):
	evento = Evento.objects.filter(fecha_baja_evento__isnull=False)
	contexto = {'eventos':evento}
	return render(request, 'evento/evento_listar_baja.html', contexto)

@login_required
def evento_edit(request, pk):
    evento = Evento.objects.get(id=pk)
    if request.method == 'GET':
        form = EventoForm(instance=evento)
    else:
        form = EventoForm(request.POST, instance=evento)
        if form.is_valid():
            form.save()
            return redirect('evento:evento_listar')
    return render(request,'evento/evento_form.html',{'form':form})

@login_required
def evento_restore(request, pk):
    evento = Evento.objects.get(id=pk)
    if request.method == 'GET':
        evento.fecha_baja_evento = None
        evento.save()
        return redirect('evento:evento_listar_baja')
    return redirect('evento:evento_listar')

@login_required
def evento_delete(request, pk):
    evento = Evento.objects.get(id=pk)
    if request.method == 'POST':
        evento.fecha_baja_evento = datetime.date.today()
        evento.save()
        return redirect('evento:evento_listar')
    return render(request,'evento/evento_delete.html',{'evento':evento})

@login_required
def buscar_evento_descripcion(request):
    query = request.GET.get('q')
    if query is not None and query != ' ' and request.is_ajax():
        eventos = get_queryset_evento(request).filter(descripcion_evento__icontains=query)
        return render(request, 'evento/buscar_evento.html', {'eventos': eventos})
    eventos =  get_queryset_evento(request)
    return render(request, 'evento/buscar_evento.html', {'eventos': eventos})
#class BuscarView(TemplateView):
#	def post(self, request, *args, **kwargs):
#		buscar = request.POST['buscalo']
#		eve1 = Evento.objects.filter(descripcion_evento__icontains=buscar)
#		eve2 = Evento.objects.filter(fecha_evento__icontains=buscar)
	#	eve3 = Evento.objects.filter(lugar_evento__icontains=buscar)
	#	eve4 = Evento.objects.filter(hora_evento__icontains=buscar)
	#	contexto = {
		#'eventos1':eve1,
		#'eventos2':eve2,
		#'eventos3':eve3,
		#'eventos4':eve4,
		#}
		#return render(request,'evento/buscar.html', contexto)
