from django.urls import include, path,re_path
from django.contrib.auth.views import login_required
from apps.evento import views
from apps.evento.views import  evento_view, evento_list, evento_edit, \
	  evento_delete, evento_list_baja, evento_restore, buscar_evento_descripcion

urlpatterns = [
    re_path(r'^evento/listar$', views.evento_list, name='evento_listar'),
    re_path(r'^evento/nueva$', views.evento_view, name='evento_crear'),
    re_path(r'^evento/editar/(?P<pk>\d+)$', views.evento_edit, name='evento_editar'),
    re_path(r'^evento/eliminar/(?P<pk>\d+)$', views.evento_delete, name='evento_eliminar'),
	re_path(r'^evento/listar/baja/$', views.evento_list_baja , name='evento_listar_baja'),
	re_path(r'^evento/restaurar/(?P<pk>\d+)/$', views.evento_restore , name='evento_restaurar'),
    re_path(r'^evento/buscarevento/$', views.buscar_evento_descripcion, name='buscarevento'),

]
