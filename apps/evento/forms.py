from django import forms

from apps.evento.models import Evento

class EventoForm(forms.ModelForm):

    class Meta:
        model = Evento

        fields = [
            'descripcion_evento',
            'fecha_evento',
            'lugar_evento',
            'hora_evento',
            #'id_ministerio_encargado'
        ]
        labels = {
            'descripcion_evento': 'Descripcion:',
            'fecha_evento': 'Dia:',
            'lugar_evento': 'Lugar:',
            'hora_evento': 'Hora:',
        }
