from django.shortcuts import render, redirect

# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.urls import reverse
import datetime
from apps.entrada.models import Entrada
from apps.salida.models import Salida
from django.contrib.auth.decorators import login_required



@login_required
def entrada_list(request):
	entrada = Entrada.objects.filter(fecha_baja_entrada__isnull=True).order_by('id')
	salida = Salida.objects.filter(fecha_baja_salida__isnull=True).order_by('id')
	contexto = {'entradas':entrada, 'salidas':salida}
	return render(request, 'entradasalida/entradasalida_listar.html', contexto)
"""
def buscar_articulo(request):
    query = request.GET.get('q')
    if query is not None and query != ' ' and request.is_ajax():
        entradasalida = get_queryset_persona(request).filter(descripcion_articulo__icontains=query)
        return render(request, 'entradasalida/buscar_entradasalida.html', {'entradasalida': entradasalida})
    personas =  get_queryset_persona(request)
    return render(request, 'entradasalida/buscar_entradasalida.html', {'entradasalida': entradasalida})
"""
