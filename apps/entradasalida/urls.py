from django.urls import include, path, re_path
from django.contrib.auth.views import login_required
from apps.entradasalida import views
from apps.entradasalida.views import entrada_list#, buscar_articulo
urlpatterns = [
    re_path(r'^entradasalida/listar$', views.entrada_list , name='entradasalida_listar'),
    #url(r'^buscararticulo/$', views.buscar_articulo, name='buscararticulo'),
]
