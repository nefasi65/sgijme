from django.urls import include, path, re_path
from django.contrib.auth.views import login_required
from apps.discipulado import views
from apps.discipulado.views import discipulado_view, discipulado_list, discipulado_edit, discipulado_delete,\
     discipulado_list_baja , discipulado_restore,buscar_discipulador_discipulado, buscar_persona_discipulado

urlpatterns = [
    re_path(r'^discipulado/listar$', views.discipulado_list, name='discipulado_listar'),
    re_path(r'^discipulado/nueva$', views.discipulado_view, name='discipulado_crear'),
    re_path(r'^discipulado/editar/(?P<pk>\d+)$', views.discipulado_edit, name='discipulado_editar'),
    re_path(r'^discipulado/eliminar/(?P<pk>\d+)$', views.discipulado_delete, name='discipulado_eliminar'),
    re_path(r'^discipulado/listar/baja/$', views.discipulado_list_baja , name='discipulado_listar_baja'),
	re_path(r'^discipulado/restaurar/(?P<pk>\d+)/$', views.discipulado_restore , name='discipulado_restaurar'),
    #url(r'^buscar/$', BuscarView.as_view(),name='discipulado_buscar'),
    re_path(r'^discipulado/buscardiscipulador/$', views.buscar_discipulador_discipulado, name='buscardiscipulador'),
    re_path(r'^discipulado/buscarpersonadicipulada/$', views.buscar_persona_discipulado, name='buscarpersonadicipulada'),
]
