from django import forms

from apps.discipulado.models import Discipulado#, Discipulador
from apps.persona.models import Persona

class DiscipuladosForm(forms.ModelForm):
    persona = forms.ModelChoiceField(queryset=Persona.objects.filter(fecha_baja_persona__isnull=True))
    discipulador = forms.ModelChoiceField(queryset=Persona.objects.filter(fecha_baja_persona__isnull=True))

    def __init__(self, *args, **kwargs):
        super(DiscipuladosForm, self).__init__(*args, **kwargs)
        self.fields['persona'].label = "Persona Discipulada"

    class Meta:
        model = Discipulado

        fields = [
            'nombre_material_discipulado',
            'discipulador',
            'persona',
            'fecha_fin',
        ]
        labels = {
            'nombre_material_discipulado': 'Nombre del material',
            'discipulador': 'Discipulador',
            'persona': 'Persona discipulada',
            'fecha_fin': 'Fecha fin discipulado',
        }

"""class DiscipuladoresForm(forms.ModelForm):

    class Meta:
        model = Discipulador

        fields = [
        #    'nombre_discipulador',
            'id_persona',
        ]
        labels = {
        #    'nombre_discipulador': 'Nombre discipulador',
            'id_persona': 'Nombre discipulador',

        }
"""
