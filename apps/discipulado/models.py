from django.db import models
#from apps.validators import persona_validation
from django.core.exceptions import ValidationError
# Create your models here.
from apps.persona.models import Persona

#class Discipulador(models.Model):
#    nombre_discipulador = models.CharField(max_length=50)#ForeignKey(Persona, null=True, blank=True, on_delete=models.CASCADE)
    #id_persona =models.ForeignKey(Persona, null=True, blank=True, related_name="personaDiscipulada", on_delete=models.CASCADE)

#    def __str__(self):
#        return self.nombre_discipulador

class Discipulado(models.Model):
    nombre_material_discipulado = models.CharField(max_length=50, blank=False, null = False)
    discipulador = models.ForeignKey(Persona, blank=False, null = False, related_name="personaDiscipuladora", on_delete=models.CASCADE)
    persona = models.ForeignKey(Persona, blank=False, null = False, related_name="personaDiscipulada", on_delete=models.CASCADE)#, validators=[persona_validation])
    fecha_fin = models.CharField(max_length=10,null=True, blank=True)
    fecha_baja_discipulado = models.DateField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.nombre_material_discipulado

    class Meta:
                permissions = (('ver_detalle_discipulado', 'Ver detalle discipulados'),)
                unique_together = ('discipulador', 'persona',)
    def clean(self):
            if self.discipulador == self.persona:
                raise ValidationError('Error: el discípulo y el discipulador no pueden ser la misma persona.')
