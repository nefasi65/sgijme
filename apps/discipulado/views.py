from django.shortcuts import render, redirect

# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.urls import reverse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, TemplateView
import datetime
from apps.discipulado.models import Discipulado#, Discipulador
from apps.discipulado.forms import DiscipuladosForm#, DiscipuladoresForm
from apps.persona.models import Persona
from django.contrib.auth.decorators import login_required
from django.db.models import Q

###################   					DISCIPULADOS


@login_required
def discipulado_view(request):
    if request.method == 'POST':
        form = DiscipuladosForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('discipulado:discipulado_listar')
    else:
        form = DiscipuladosForm
    return render(request, 'discipulado/discipulado_form.html', {'form':form})

@login_required
def discipulado_list(request):
	discipulado = Discipulado.objects.filter(fecha_baja_discipulado__isnull=True)
	contexto = {'discipulados':discipulado}
	return render(request, 'discipulado/discipulado_listar.html', contexto)

@login_required
def get_queryset_discipulado(self):
    queryset = Discipulado.objects.filter(fecha_baja_discipulado__isnull=True).order_by('nombre_material_discipulado')#
    return queryset

@login_required
def discipulado_list_baja(request):
	discipulado = Discipulado.objects.filter(fecha_baja_discipulado__isnull=False)
	contexto = {'discipulados':discipulado}
	return render(request, 'discipulado/discipulado_listar_baja.html', contexto)

@login_required
def discipulado_restore(request, pk):
    discipulado = Discipulado.objects.get(id=pk)
    if request.method == 'GET':
        discipulado.fecha_baja_discipulado = None
        discipulado.save()
        return redirect('discipulado:discipulado_listar_baja')
    return redirect('discipulado:discipulado_listar')

@login_required
def discipulado_edit(request, pk):
    discipulado = Discipulado.objects.get(id=pk)
    if request.method == 'GET':
        form = DiscipuladosForm(instance=discipulado)
    else:
        form = DiscipuladosForm(request.POST, instance=discipulado)
        if form.is_valid():
            form.save()
            return redirect('discipulado:discipulado_listar')
    return render(request,'discipulado/discipulado_form.html',{'form':form})

@login_required
def discipulado_delete(request, pk):
	discipulado = Discipulado.objects.get(id=pk)
	if request.method == 'POST':
		discipulado.fecha_baja_discipulado = datetime.date.today()
		discipulado.save()
		return redirect('discipulado:discipulado_listar')
	return render(request,'discipulado/discipulado_delete.html',{'discipulado':discipulado})

@login_required
def buscar_discipulador_discipulado(request):
    query = request.GET.get('q')
    if query is not None and query != ' ' and request.is_ajax():
        discipulados = get_queryset_discipulado(request).filter(Q(discipulador__nombre__icontains=query)|Q(discipulador__apellido_persona__icontains=query))
        return render(request, 'discipulado/buscar_discipulado.html', {'discipulados': discipulados})
    discipulados =  get_queryset_discipulado(request)
    return render(request, 'discipulado/buscar_discipulado.html', {'discipulados': discipulados})

@login_required
def buscar_persona_discipulado(request):
    query = request.GET.get('q')
    if query is not None and query != ' ' and request.is_ajax():
        discipulados = get_queryset_discipulado(request).filter(Q(persona__nombre__icontains=query)|Q(persona__apellido_persona__icontains=query))
        return render(request, 'discipulado/buscar_discipulado.html', {'discipulados': discipulados})
    discipulados =  get_queryset_discipulado(request)
    return render(request, 'discipulado/buscar_discipulado.html', {'discipulados': discipulados})

#class BuscarView(TemplateView):
#	def post(self, request, *args, **kwargs):
#		buscar = request.POST['buscalo']
#		dis1 = Discipulado.objects.filter(nombre_material_discipulado__icontains=buscar)
#		dis2 = Discipulado.objects.filter(discipulador__nombre__icontains=buscar)
#		dis3 = Discipulado.objects.filter(persona__nombre__icontains=buscar)
#		dis4 = Discipulado.objects.filter(fecha_fin__icontains=buscar)
#		contexto = {
#		'discipulados1':dis1,
#		'discipulados2':dis2,
#		'discipulados3':dis3,
#		'discipulados4':dis4,
#		}
#		return render(request,'discipulado/buscar.html', contexto)
