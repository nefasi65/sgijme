from django.urls import path, re_path, include#, url
from .views import RegistroUsuario
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [

	re_path(r'^$' , LoginView.as_view(template_name='persona/index.html'), name="login"),
	#views.LoginView.as_view(template_name='persona/index.html'), name='login'),
	re_path(r'^$' , LogoutView.as_view(), name="logout"),
	#path(r'^cerrar/$' , 'django.contrib.auth.views.logout_then_login', name='logout'),

	re_path(r'^registrarse/$', RegistroUsuario.as_view() , name ='registrarse'),


]
