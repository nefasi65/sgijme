from django import forms

from apps.celula.models import Celula, MiembrosCelula, Persona

class CelulaForm(forms.ModelForm):
    encargado_celula = forms.ModelChoiceField(queryset=Persona.objects.filter(fecha_baja_persona__isnull=True))
    def __init__(self, *args, **kwargs):
        super(CelulaForm, self).__init__(*args, **kwargs)
        self.fields['encargado_celula'].label = "Encargado"
    class Meta:
        model = Celula

        fields = [
            'dia_celula',
            'hora_celula',
            'direccion_celula',
            'encargado_celula',
        ]
        labels = {
            'dia_celula': 'Dia',
            'hora_celula': 'Hora',
            'direccion_celula': 'Dirección',
            'encargado_celula': 'Encargado',
        }

class MiembrosCelulaForm(forms.ModelForm):
    miembro = forms.ModelChoiceField(queryset=Persona.objects.filter(fecha_baja_persona__isnull=True))
    def __init__(self, *args, **kwargs):
        super(MiembrosCelulaForm, self).__init__(*args, **kwargs)
        self.fields['miembro'].label = "Miembro"
        self.fields['celula'].label = "Celula"

    class Meta:
        model = MiembrosCelula

        fields = [
            'celula',
            'miembro',
            'fecha_ingreso',
            'comentario',
        ]
        labels = {
            'celula': 'Celula',
            'miembro': 'Miembro',
            'fecha_ingreso': 'Fecha de ingreso',
            'comentario': 'Comentario',
        }
