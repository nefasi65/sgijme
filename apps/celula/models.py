from django.db import models

# Create your models here.
from apps.persona.models import Persona

dias_semana_choices = (
    ('Lunes', 'Lunes'),
    ('Martes', 'Martes'),
    ('Miércoles', 'Miércoles'),
    ('Jueves', 'Jueves'),
    ('Viernes', 'Viernes'),
    ('Sábado', 'Sábado'),
    ('Domingo', 'Domingo'),
)

class Celula(models.Model):
    dia_celula = models.CharField(max_length=10, blank=False, choices = dias_semana_choices)
    hora_celula = models.TimeField(null=True, blank=False)
    direccion_celula = models.CharField(max_length=50, blank=False)
    encargado_celula = models.ForeignKey(Persona, null=False, blank=False, related_name = 'encargado_celula', on_delete=models.CASCADE)
    miembros_celula = models.ManyToManyField(Persona, through='MiembrosCelula')
    fecha_baja_celula = models.DateField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.direccion_celula

    class Meta:
                permissions = (('ver_detalle_celula', 'Ver detalle celulas'),)
                unique_together = (('dia_celula','hora_celula','encargado_celula',),('dia_celula','hora_celula','direccion_celula',))

    def unique_error_message(self, model_class, unique_check):
        error = super().unique_error_message(model_class, unique_check)
        # Intercept the unique_together error
        if len(unique_check) != 1:
            error.message = 'Encargado o dirección no disponible en este día y horario.'
            return error

class MiembrosCelula(models.Model):
    miembro =  models.ForeignKey(Persona, on_delete=models.CASCADE)
    celula = models.ForeignKey(Celula, on_delete=models.CASCADE)
    fecha_ingreso = models.CharField(max_length=30)
    comentario = models.CharField(max_length=10, blank=True, null=True)
    fecha_baja_miembro_celula = models.DateField(max_length=10, blank=True, null=True)

    def __str__(self):
        return self.miembro

    class Meta:
                permissions = (('ver_detalle_miembros_celula', 'Ver detalle miembos de celulas'),)
                unique_together = ("miembro","celula"),
