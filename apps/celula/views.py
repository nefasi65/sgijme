from django.shortcuts import render, redirect

# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.urls import reverse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, TemplateView
import datetime
from apps.celula.forms import CelulaForm, MiembrosCelulaForm
from .models import Celula, MiembrosCelula
from django.db.models import Q
from django.contrib.auth.decorators import login_required

@login_required
def celula_view(request):
    if request.method == 'POST':
        form = CelulaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('celula:celula_listar')
    else:
        form = CelulaForm
    return render(request, 'celula/celula_form.html', {'form':form})

@login_required
def celula_list(request):
	celula = Celula.objects.filter(fecha_baja_celula__isnull=True)
	contexto = {'celulas':celula}
	return render(request, 'celula/celula_listar.html', contexto)

@login_required
def get_queryset_celula(self):
    queryset = Celula.objects.filter(fecha_baja_celula__isnull=True).order_by('hora_celula')
    return queryset

@login_required
def celula_list_baja(request):
	celula = Celula.objects.filter(fecha_baja_celula__isnull=False)
	contexto = {'celulas':celula}
	return render(request, 'celula/celula_listar_baja.html', contexto)

@login_required
def celula_edit(request, pk):
    celula = Celula.objects.get(id=pk)
    if request.method == 'GET':
        form = CelulaForm(instance=celula)
    else:
        form = CelulaForm(request.POST, instance=celula)
        if form.is_valid():
            form.save()
            return redirect('celula:celula_listar')
    return render(request,'celula/celula_form.html',{'form':form})

@login_required
def celula_delete(request, pk):
    celula = Celula.objects.get(id=pk)
    if request.method == 'POST':
	    celula.fecha_baja_celula = datetime.date.today()
	    celula.save()
	    return redirect('celula:celula_listar')
    return render(request,'celula/celula_delete.html',{'celula':celula})

@login_required
def celula_restore(request, pk):
	celula = Celula.objects.get(id=pk)
	if request.method == 'GET':
		celula.fecha_baja_celula = None
		celula.save()
		return redirect('celula:celula_listar_baja')
	return redirect('celula:celula_listar')

@login_required
def buscar_dia_celula(request):
    query = request.GET.get('q')
    if query is not None and query != ' ' and request.is_ajax():
        celulas = get_queryset_celula(request).filter(dia_celula__icontains=query)
        return render(request, 'celula/buscar_celula.html', {'celulas': celulas})
    celulas =  get_queryset_celula(request)
    return render(request, 'celula/buscar_celula.html', {'celulas': celulas})

@login_required
def buscar_encargado_celula(request):
    query = request.GET.get('q')
    if query is not None and query != ' ' and request.is_ajax():
        celulas = get_queryset_celula(request).filter(Q(encargado_celula__nombre__icontains=query)|Q(encargado_celula__apellido_persona__icontains=query))
        return render(request, 'celula/buscar_celula.html', {'celulas': celulas})
    celulas =  get_queryset_celula(request)
    return render(request, 'celula/buscar_celula.html', {'celulas': celulas})

#MIEMBROS
@login_required
def celula_agregar_miembros(request):
	if request.method == 'POST':
		form = MiembrosCelulaForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('celula:celula_agregar_miembros')
	else:
		form = MiembrosCelulaForm
	return render(request, 'celula/celula_agregar_miembros.html', {'form':form})

@login_required
def miembros_celula_list(request, pk):
	miembro_celula = MiembrosCelula.objects.filter(celula_id=pk, fecha_baja_miembro_celula__isnull=True)
	celula = Celula.objects.filter(id=pk)
	contexto = {'miembros_celula':miembro_celula,
				'celulas':celula}
	return render(request, 'celula/miembros_celula_listar.html', contexto)

@login_required
def miembros_celula_list_baja(request, pk):
	miembro_celula = MiembrosCelula.objects.filter(celula_id=pk, fecha_baja_miembro_celula__isnull=False)
	contexto = {'miembros_celula':miembro_celula}
	return render(request, 'celula/miembros_celula_listar_baja.html', contexto)

@login_required
def miembros_celula_delete(request, pk):
    miembro_celula = MiembrosCelula.objects.get(id=pk)
    if request.method == 'POST':
	    miembro_celula.fecha_baja_miembro_celula = datetime.date.today()
	    miembro_celula.save()
	    return redirect('celula:miembros_celula_listar', miembro_celula.celula_id)
    return render(request,'celula/miembros_celula_delete.html',{'miembros_celula':miembro_celula})

@login_required
def miembros_celula_restore(request, pk):
    miembro_celula = MiembrosCelula.objects.get(id=pk)
    if request.method == 'GET':
        miembro_celula.fecha_baja_miembro_celula = None
        miembro_celula.save()
        return redirect('celula:miembros_celula_listar_baja', miembro_celula.celula_id)
    return redirect('celula:miembros_celula_listar', miembro_celula.celula_id)
#
@login_required
class BuscarView(TemplateView):
	def post(self, request, *args, **kwargs):
		buscar = request.POST['buscalo']
		cel1 = Celula.objects.filter(dia_celula__icontains=buscar)
		cel2 = Celula.objects.filter(hora_celula__icontains=buscar)
		cel3 = Celula.objects.filter(direccion_celula__icontains=buscar)
		cel4 = Celula.objects.filter(encargado_celula__nombre__icontains=buscar)
		contexto = {
		'celulas1':cel1,
		'celulas2':cel2,
		'celulas3':cel3,
		'celulas4':cel4,
		}
		return render(request,'celula/buscar.html', contexto)
