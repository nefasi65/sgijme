from django.urls import include, path, re_path
from django.contrib.auth.views import login_required
from apps.celula import views
from apps.celula.views import celula_view, celula_list, celula_edit, \
celula_delete, celula_list_baja, celula_restore, BuscarView, celula_agregar_miembros, \
miembros_celula_list, miembros_celula_delete, miembros_celula_list_baja, buscar_dia_celula, buscar_encargado_celula

urlpatterns = [
    re_path(r'^celula/nueva$', views.celula_view , name='celula_crear'),
    re_path(r'^celula/listar$', views.celula_list , name='celula_listar'),
    re_path(r'^celula/editar/(?P<pk>\d+)$', views.celula_edit , name='celula_editar'),
    re_path(r'^celula/eliminar/(?P<pk>\d+)$', views.celula_delete , name='celula_eliminar'),
    re_path(r'^celula/listar/agregar/$', views.celula_agregar_miembros , name='celula_agregar_miembros'),
    re_path(r'^celula/listar/agregar/detalle/(?P<pk>\d+)$', views.miembros_celula_list , name='miembros_celula_listar'),
    re_path(r'^celula/listar/agregar/detalle/baja/listar/(?P<pk>\d+)$', views.miembros_celula_list_baja , name='miembros_celula_listar_baja'),
    re_path(r'^celula/listar/agregar/detalle/baja/(?P<pk>\d+)$', views.miembros_celula_delete , name='miembros_celula_eliminar'),
    re_path(r'^celula/listar/agregar/detalle/baja/restaurar/(?P<pk>\d+)$', views.miembros_celula_restore , name='miembros_celula_restaurar'),
	re_path(r'^celula/listar/baja/$', views.celula_list_baja , name='celula_listar_baja'),
	re_path(r'^celula/restaurar/(?P<pk>\d+)/$', views.celula_restore , name='celula_restaurar'),
    re_path(r'^celula/buscardiacelula/$', views.buscar_dia_celula, name='buscardiacelula'),
    re_path(r'^celula/buscarencargadocelula/$', views.buscar_encargado_celula, name='buscarencargadocelula'),
]
