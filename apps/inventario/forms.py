from django import forms

from apps.inventario.models import Articulo#,Unidadad_de_Medida,

class ArticuloForm(forms.ModelForm):

    class Meta:
        model = Articulo

        fields = [
            'descripcion_articulo',
            'cantidad_articulo',
            'nombre_unidad',
            'precio_Compra_articulo',
        ]
        labels = {
            'descripcion_articulo': 'Descripcion Inventario',
            'cantidad_articulo': 'Cantidad',
            'nombre_unidad': 'Unidad de Medida',
            'precio_Compra_articulo': 'Precio Unitario',

        }
"""
class Unidadad_de_MedidaForm(forms.ModelForm):
    class Meta:
        model = Unidadad_de_Medida
        fields = [
                'nombre_unidad'
        ]
        labels = {
                'nombre_unidad':'Nombre Unidad de Medida'
        }
"""
