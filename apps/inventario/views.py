from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.urls import reverse
##from django.views.generic import ListView, CreateView, UpdateView, DeleteView, TemplateView
from django.views.generic import TemplateView
import datetime
##from apps.inventario.models import Unidadad_de_Medida, Articulo
##from apps.inventario.forms import ArticuloForm, Unidadad_de_MedidaForm
from django.contrib.auth.decorators import login_required
from apps.persona.models import Persona
from apps.inventario.forms import ArticuloForm
from django.db.models import Q
from .models import Articulo


@login_required
def articulo_view(request):
    if request.method == 'POST':
        form = ArticuloForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('articulo:articulo_crear')
    else:
        form = ArticuloForm
    return render(request, 'articulo/articulo_form.html', {'form':form})

@login_required
def articulo_list(request):
	articulos = Articulo.objects.filter(fecha_baja_articulo__isnull=True)
	contexto = {'articulos':articulos, 'total':calcular_total2(articulos)}
	return render(request, 'articulo/articulo_listar.html', contexto)

def calcular_total2(articulos):
    total = 0
    totalentrada = 0
    for a in articulos:
       total = a.precio_Compra_articulo*a.cantidad_articulo + total
    return total

@login_required
def articulo_list_baja(request):
	articulo = Articulo.objects.filter(fecha_baja_articulo__isnull=False)
	contexto = {'articulos':articulo}
	return render(request, 'articulo/articulo_listar_baja.html', contexto)

@login_required
def articulo_edit(request, pk):
    articulo = Articulo.objects.get(id=pk)
    if request.method == 'GET':
        form = ArticuloForm(instance=articulo)
    else:
        form = ArticuloForm(request.POST, instance=articulo)
        if form.is_valid():
            form.save()
            return redirect('articulo:articulo_listar')
    return render(request,'articulo/articulo_form.html',{'form':form})

@login_required
def articulo_delete(request, pk):
    articulo = Articulo.objects.get(id=pk)
    if request.method == 'POST':
        articulo.fecha_baja_articulo = datetime.date.today()
        articulo.save()
        return redirect('articulo:articulo_listar')
    return render(request,'articulo/articulo_delete.html',{'articulo':articulo})

@login_required
def articulo_restore(request, pk):
    articulo = Articulo.objects.get(id=pk)
    if request.method == 'GET':
        articulo.fecha_baja_articulo = None
        articulo.save()
        return redirect('articulo:articulo_listar')
    return redirect('articulo:articulo_listar_baja')

@login_required
def get_queryset_articulo(self):
    queryset = Articulo.objects.filter(fecha_baja_articulo__isnull=True).order_by('descripcion_articulo')
    return queryset

@login_required
def buscar_articulo(request):
    query = request.GET.get('q')
    if query is not None and query != ' ' and request.is_ajax():
        articulos = get_queryset_articulo(request).filter(descripcion_articulo__icontains=query)
        return render(request, 'articulo/buscar_articulo.html', {'articulos': articulos, 'total2':calcular_total2(articulos) })
    articulos =  get_queryset_articulo(request)
    return render(request, 'articulo/buscar_articulo.html', {'articulos': articulos,'total2':calcular_total2(articulos)})

#Categorias
"""
def unidad_de_medida_view(request):
    if request.method == 'POST':
        form = Unidadad_de_MedidaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('unidad_de_medida:unidad_de_medida_listar')
    else:
        form = Unidadad_de_MedidaForm
    return render(request, 'unidad_de_medida/unidad_de_medida_form.html', {'form':form})


def unidad_de_medida_list(request):
	categoria = Unidadad_de_Medida.objects.all().order_by('id')
	contexto = {'unidad_de_medida':unidad_de_medida}
	return render(request, 'unidad_de_medida/unidad_de_medida_list.html', contexto)

def unidad_de_medida_edit(request, id_unidad_de_medida):
    unidad_de_medida = Unidadad_de_Medida.objects.get(id=id_unidad_de_medida)
    if request.method == 'GET':
        form = Unidadad_de_MedidaForm(instance=unidad_de_medida)
    else:
        form = Unidadad_de_MedidaForm(request.POST, instance=unidad_de_medida)
        if form.is_valid():
            form.save()
            return redirect('unidad_de_medida:unidad_de_medida_listar')
    return render(request,'unidad_de_medida/unidad_de_medida_form.html',{'form':form})

def unidad_de_medida_delete(request, id_categoria):
    unidad_de_medida = Unidadad_de_Medida.objects.get(id=id_unidad_de_medida)
    if request.method == 'POST':
        categoria.delete()
        return redirect('unidad_de_medida:unidad_de_medida_listar')
    return render(request,'unidad_de_medida/unidad_de_medida_delete.html',{'unidad_de_medida':unidad_de_medida})




class Unidadad_de_MedidaList(ListView):
	model = Unidadad_de_Medida
	template_name = 'unidad_de_medida/unidad_de_medida_list.html'
	paginate_by = 3

class Unidadad_de_MedidaCreate(CreateView):
	model = Unidadad_de_Medida
	form_class = Unidadad_de_MedidaForm
	template_name = 'unidad_de_medida/unidad_de_medida_form.html'
	success_url = reverse_lazy('unidad_de_medida:unidad_de_medida_listar')

class Unidadad_de_MedidaUpdate(UpdateView):
	model = Unidadad_de_Medida
	form_class = Unidadad_de_MedidaForm
	template_name = 'unidad_de_medida/unidad_de_medida_form.html'
	success_url = reverse_lazy('unidad_de_medida:unidad_de_medida_listar')


class Unidadad_de_MedidaDelete(DeleteView):
	model = Unidadad_de_Medida
	template_name = 'unidad_de_medida/unidad_de_medida_delete.html'
	success_url = reverse_lazy('unidad_de_medida:unidad_de_medida_listar')

"""
@login_required
class BuscarView(TemplateView):
	def post(self, request, *args, **kwargs):
		buscar = request.POST['buscalo']
		art1 = Articulo.objects.filter(descripcion_articulo__contains=buscar)
		art2 = Articulo.objects.filter(precio_Compra_articulo__contains=buscar)
		contexto = {
		'articulos1':art1,
		'articulos2':art2,
		}
		return render(request,'articulo/buscar.html', contexto)
