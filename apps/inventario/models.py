from django.db import models
import decimal
from django.core.validators import MinValueValidator

unidad_de_medida__choices = (
    ('Caja', 'Caja'),
    ('Cm', 'Centímetros'),
    ('Decena', 'Decena'),
    ('Docena', 'Docena'),
    ('G', 'Gramos'),
    ('Kg', 'Kilogramos'),
    ('Lts', 'Litros'),
    ('Mts', 'Metros'),
    ('Pallets', 'Pallets'),
    ('Unidad', 'Unidad'),
)


"""class Unidadad_de_Medida(models.Model):
    nombre_unidad = models.CharField(max_length=50)
    def __str__(self):
        return self.nombre_unidad
"""
class Articulo(models.Model):
    descripcion_articulo = models.CharField(max_length=50)
    cantidad_articulo = models.PositiveSmallIntegerField(validators=[MinValueValidator(0)], default=0)
    nombre_unidad = models.CharField(max_length=15, choices=unidad_de_medida__choices,)
    precio_Compra_articulo = models.DecimalField(max_digits=8, decimal_places=2, default=0.00)
    fecha_baja_articulo = models.DateField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.descripcion_articulo

    def preeciototal(self):
	    precio_total=self.precio_Compra_articulo*self.cantidad_articulo
	    return precio_total

    class Meta:
                permissions = (('ver_detalle_inventario', 'Ver detalle inventarios'),)
