from django.urls import include, path, re_path
from django.contrib.auth.views import login_required
from apps.inventario import views
from apps.inventario.views import  articulo_view, articulo_list, articulo_list_baja, articulo_edit, articulo_delete, articulo_restore, \
	 BuscarView, buscar_articulo

urlpatterns = [
    re_path(r'^articulo/nueva$', views.articulo_view , name='articulo_crear'),
    re_path(r'^articulo/listar$', views.articulo_list , name='articulo_listar'),
    re_path(r'^articulo/editar/(?P<pk>\d+)$', views.articulo_edit , name='articulo_editar'),
    re_path(r'^articulo/eliminar/(?P<pk>\d+)$', views.articulo_delete , name='articulo_eliminar'),
	re_path(r'^articulo/listar/baja/$', views.articulo_list_baja , name='articulo_listar_baja'),
	re_path(r'^articulo/restaurar/(?P<pk>\d+)/$', views.articulo_restore , name='articulo_restaurar'),
	#url(r'^nuevo_unidad_de_medida$',login_required(Unidadad_de_MedidaCreate.as_view()), name='unidad_de_medida_crear'),
    #url(r'^listar_unidad_de_medida$', login_required(Unidadad_de_MedidaList.as_view()), name='unidad_de_medida_listar'),
    #url(r'^editar_unidad_de_medida/(?P<pk>\d+)/$', login_required(Unidadad_de_MedidaUpdate.as_view()), name='unidad_de_medida_editar'),
    #url(r'^eliminar_unidad_de_medida/(?P<pk>\d+)/$', login_required(Unidadad_de_MedidaDelete.as_view()), name='unidad_de_medida_eliminar'),
	re_path(r'^buscar/$', views.BuscarView,name='buscar'),
	re_path(r'^articulo/buscararticulo/$', views.buscar_articulo, name='buscararticulo'),
]
