from django import forms

from apps.movimiento_dinero.models import Movimiento_Dinero
from apps.persona.models import Persona
from apps.evento.models import Evento
from apps.ministerio.models import Ministerios

class Movimiento_DineroForm(forms.ModelForm):
    id_persona = forms.ModelChoiceField(required=True, queryset=Persona.objects.filter(fecha_baja_persona__isnull=True))
    id_evento = forms.ModelChoiceField(required=False, queryset=Evento.objects.filter(fecha_baja_evento__isnull=True))
    id_ministerio = forms.ModelChoiceField(required=False, queryset=Ministerios.objects.filter(fecha_baja_ministerio__isnull=True))
    ingreso= forms.DecimalField(required=False, max_digits=8, decimal_places=2)
    egreso= forms.DecimalField(required=False, max_digits=8, decimal_places=2)

    def __init__(self, *args, **kwargs):
        super(Movimiento_DineroForm, self).__init__(*args, **kwargs)
        self.fields['id_persona'].label = "Autor"
        self.fields['id_evento'].label = "Evento"
        self.fields['id_ministerio'].label = "Ministerio"


    class Meta:
        model = Movimiento_Dinero

        fields = [
            'fecha_movimiento',
            'clase_movimiento',
            'categoria',
            'ingreso',
            'egreso',
            'descripcion_movimiento',
            'id_persona',
            'id_evento',
            #'id_matrimonio',
            'id_ministerio',
            'comentario',
        ]
        labels = {
            'fecha_movimiento': 'Fecha Movimiento',
            'clase_movimiento': 'Fijo/Eventual',
            'categoria': 'Categoría',
            'ingreso': 'Ingreso',
            'egreso': 'Egreso',
            'descripcion_movimiento': 'Descripcion',
            'id_persona': 'Autor',
            'id_evento': 'Evento',
            #'id_matrimonio': 'Matrimonio',
            'id_ministerio': 'Ministerio',
            'comentario': 'Comentario',
        }

"""class CategoriaForm(forms.ModelForm):
    class Meta:
        model = Categoria
        fields = [
                'nombre_categoria'
        ]
        labels = {
                'nombre_categoria':'Nombre Categoria',
        }
"""
