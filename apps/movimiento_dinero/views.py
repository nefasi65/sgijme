from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.urls import reverse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, TemplateView
from datetime import datetime, date
from apps.movimiento_dinero.forms import Movimiento_DineroForm#, CategoriaForm
from apps.persona.forms import PersonaForm
from apps.persona.models import Persona
from apps.evento.models import Evento
from apps.ministerio.models import Ministerios
from .models import Movimiento_Dinero
from django.db.models import Q
from django.contrib.auth.decorators import login_required
#Vista genérica para mostrar resultados
from django.views.generic.base import TemplateView
#Workbook nos permite crear libros en excel
from openpyxl import Workbook
#Nos devuelve un objeto resultado, en este caso un archivo de excel
from django.http.response import HttpResponse
from openpyxl.styles import Border, Side
from django.core.exceptions import ValidationError
from django.contrib import messages

#Informe PDF
from django.conf import settings
from io import BytesIO
from reportlab.pdfgen import canvas
from django.views.generic import View
from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle, Table, Image, Spacer
from reportlab.lib.units import cm
from reportlab.lib.pagesizes import A4,letter, landscape
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.enums import TA_JUSTIFY, TA_LEFT, TA_CENTER
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle, TA_CENTER
from reportlab.lib.units import inch, mm
from reportlab.lib import colors
from reportlab.platypus import (
        Paragraph,
        Table,
        SimpleDocTemplate,
        Spacer,
        TableStyle,
        Paragraph)

@login_required
def movimiento_view(request):
    if request.method == 'POST':
        form = Movimiento_DineroForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('movimiento:movimiento_listar')
    else:
        form = Movimiento_DineroForm
    return render(request, 'movimiento_dinero/movimiento_dinero_form.html', {'form':form})

""""def movimiento_list(request):
	movimiento = Movimiento_Dinero.objects.filter(fecha_baja_movimiento_dinero__isnull=True)
	total2 = 0
	for a in movimiento:
		if a.tipo_movimiento == "Ingreso":
			total2 = a.monto_movimiento + total2
		else:
			total2 = total2 - a.monto_movimiento
	contexto = {'movimientos':movimiento, 'total2':total2}
	return render(request, 'movimiento_dinero/movimiento_dinero_list.html', contexto)"""
@login_required
def movimiento_list(request):
    movimiento = Movimiento_Dinero.objects.filter(fecha_baja_movimiento_dinero__isnull=True).order_by('id')#.order_by('-fecha_movimiento')
    contexto = {'movimientos':movimiento, 'total2':calcular_total(movimiento)}
    return render(request, 'movimiento_dinero/movimiento_dinero_list.html', contexto)

def calcular_total(movimiento):
    total2 = 0
    for a in movimiento:
		    total2 = a.ingreso + total2
		    total2 = total2 - a.egreso
    return total2

@login_required
def get_queryset_movimiento_dinero(self):
    queryset = Movimiento_Dinero.objects.filter(fecha_baja_movimiento_dinero__isnull=True).order_by('fecha_movimiento')#
    return queryset

@login_required
def get_queryset_movimiento_id(self):
    queryset = Movimiento_Dinero.objects.filter(fecha_baja_movimiento_dinero__isnull=True).order_by('fecha_movimiento')#
    return queryset

@login_required
def movimiento_list_baja(request):
	movimiento = Movimiento_Dinero.objects.filter(fecha_baja_movimiento_dinero__isnull=False)
	contexto = {'movimientos':movimiento}
	return render(request, 'movimiento_dinero/movimiento_dinero_list_baja.html', contexto)

#def movimiento_edit(request, pk):
#    movimiento = Movimiento_Dinero.objects.get(id=pk)
#    if request.method == 'GET':
#        form = Movimiento_DineroForm(instance=movimiento)
#    else:
#        form = Movimiento_DineroForm(request.POST, instance=movimiento)
#        if form.is_valid():
#            form.save()
#            return redirect('movimiento:movimiento_listar')
#    return render(request,'movimiento_dinero/movimiento_dinero_form.html',{'form':form})

@login_required
def movimiento_delete(request, pk):
	movimiento = Movimiento_Dinero.objects.get(id=pk)
	if request.method == 'POST':
		movimiento.fecha_baja_movimiento_dinero = date.today()
		movimiento.save()
		return redirect('movimiento:movimiento_listar')
	return render(request,'movimiento_dinero/movimiento_dinero_delete.html',{'movimiento':movimiento})

@login_required
def movimiento_buscar(request):
    movimiento = buscar(request)
    """
    movimiento = Movimiento_Dinero.objects.all()
    autor = request.GET.get('autor')
    ministerio = request.GET.get('ministerio')
    id_movimiento = request.GET.get('id_movimiento')
    fechadesde = request.GET.get('fechadesde')
    fechahasta = request.GET.get('fechahasta')
    tipomovimiento = request.GET.get('tipomovimiento')
    claseomovimiento = request.GET.get('claseomovimiento')

    if autor != '' and autor is not None:
        movimiento = movimiento.filter(Q(id_persona__nombre__icontains=autor)|Q(id_persona__apellido_persona__icontains=autor)).order_by('fecha_movimiento')
    if ministerio != '' and ministerio is not None:
        movimiento = movimiento.filter(id_ministerio__nombre_ministerio__icontains=ministerio).order_by('-fecha_movimiento')
    if id_movimiento != '' and id_movimiento is not None:
        movimiento = movimiento.filter(id__exact=id_movimiento).order_by('-fecha_movimiento')

    if tipomovimiento != '' and tipomovimiento == "Ingreso" and tipomovimiento is not None:
        movimiento = movimiento.filter(egreso=0).order_by('-fecha_movimiento')
    elif tipomovimiento != '' and tipomovimiento == "Egreso" and tipomovimiento is not None:
        movimiento = movimiento.filter(ingreso=0).order_by('-fecha_movimiento')

    if claseomovimiento != '' and claseomovimiento != "Todo" and claseomovimiento is not None:
        movimiento = movimiento.filter(clase_movimiento__icontains=claseomovimiento).order_by('-fecha_movimiento')

    if fechadesde != '' and fechadesde is not None:
        if fechahasta != '' and fechahasta is not None:
            fechadesdeformateada = datetime.strptime(fechadesde, "%d/%m/%Y").date()
            fechahastaformateada = datetime.strptime(fechahasta, "%d/%m/%Y").date()
            if fechahastaformateada >= fechadesdeformateada:
                movimiento = movimiento.filter(fecha_movimiento__range=[fechadesdeformateada,fechahastaformateada]).order_by('-fecha_movimiento')
            else:
                messages.error(request,'Fecha Hasta no puede ser menor que Fecha Desde')
        else:
            messages.error(request,'Fecha Desde y Fecha Hasta no pueden estar en blanco')
    """
    contexto = {'movimientos':movimiento,'total2':calcular_total(movimiento)}
    return render(request, 'movimiento_dinero/movimiento_dinero_list.html', contexto)

def buscar(request):
    movimiento = Movimiento_Dinero.objects.filter(fecha_baja_movimiento_dinero__isnull=True)
    autor = request.GET.get('autor')
    ministerio = request.GET.get('ministerio')
    id_movimiento = request.GET.get('id_movimiento')
    fechadesde = request.GET.get('fechadesde')
    fechahasta = request.GET.get('fechahasta')
    tipomovimiento = request.GET.get('tipomovimiento')
    claseomovimiento = request.GET.get('claseomovimiento')
    if autor != '' and autor is not None:
        movimiento = movimiento.filter(Q(id_persona__nombre__icontains=autor)|Q(id_persona__apellido_persona__icontains=autor)).order_by('fecha_movimiento')
    if ministerio != '' and ministerio is not None:
        movimiento = movimiento.filter(id_ministerio__nombre_ministerio__icontains=ministerio).order_by('-fecha_movimiento')
    if id_movimiento != '' and id_movimiento is not None:
        movimiento = movimiento.filter(id__exact=id_movimiento).order_by('-fecha_movimiento')
    if tipomovimiento != '' and tipomovimiento == "Ingreso" and tipomovimiento is not None:
        movimiento = movimiento.filter(egreso=0).order_by('-fecha_movimiento')
    elif tipomovimiento != '' and tipomovimiento == "Egreso" and tipomovimiento is not None:
        movimiento = movimiento.filter(ingreso=0).order_by('-fecha_movimiento')

    if claseomovimiento != '' and claseomovimiento != "Todo" and claseomovimiento is not None:
        movimiento = movimiento.filter(clase_movimiento__icontains=claseomovimiento).order_by('-fecha_movimiento')

    if fechadesde != '' and fechadesde is not None:
        if fechahasta != '' and fechahasta is not None:
            fechadesdeformateada = datetime.strptime(fechadesde, "%d/%m/%Y").date()
            fechahastaformateada = datetime.strptime(fechahasta, "%d/%m/%Y").date()
            if fechahastaformateada >= fechadesdeformateada:
                movimiento = movimiento.filter(fecha_movimiento__range=[fechadesdeformateada,fechahastaformateada]).order_by('-fecha_movimiento')
            else:
                messages.error(request,'Fecha Hasta no puede ser menor que Fecha Desde')
        else:
            messages.error(request,'Fecha Desde y Fecha Hasta no pueden estar en blanco')
    return movimiento

"""def buscar_autor_movimiento_dinero(request):
    query = request.GET.get('q')
    if query is not None and query != ' ' and request.is_ajax():
        movimientos = get_queryset_movimiento_dinero(request).filter(Q(id_persona__nombre__icontains=query)|Q(id_persona__apellido_persona__icontains=query))
        return render(request, 'movimiento_dinero/buscar_movimiento_dinero.html', {'movimientos': movimientos})
    movimientos =  get_queryset_movimiento_dinero(request)
    return render(request, 'movimiento_dinero/buscar_movimiento_dinero.html', {'movimientos': movimientos})


@login_required
def buscar_por_tipo_movimiento(request):
    query = request.GET.get('q')
    if query is not None and query == 'Ingreso' and request.is_ajax():
        movimientos = get_queryset_movimiento_dinero(request).filter(egreso=0)#tipo_movimiento__icontains=query)
        return render(request, 'movimiento_dinero/buscar_movimiento_dinero.html', {'movimientos': movimientos})
    elif query is not None and query == 'Egreso' and request.is_ajax():
        movimientos = get_queryset_movimiento_dinero(request).filter(ingreso=0)#tipo_movimiento__icontains=query)
        return render(request, 'movimiento_dinero/buscar_movimiento_dinero.html', {'movimientos': movimientos})
    movimientos =  get_queryset_movimiento_dinero(request)
    return render(request, 'movimiento_dinero/buscar_movimiento_dinero.html', {'movimientos': movimientos})

@login_required
def buscar_por_clase_movimiento(request):
    query = request.GET.get('q')
    if query is not None and query != ' ' and request.is_ajax():
        movimientos = get_queryset_movimiento_dinero(request).filter(clase_movimiento__icontains=query)
        return render(request, 'movimiento_dinero/buscar_movimiento_dinero.html', {'movimientos': movimientos})
    movimientos =  get_queryset_movimiento_dinero(request)
    return render(request, 'movimiento_dinero/buscar_movimiento_dinero.html', {'movimientos': movimientos})

@login_required
def buscar_por_ministerio(request):
    query = request.GET.get('q')
    if query is not None and query != ' ' and request.is_ajax():
        movimientos = get_queryset_movimiento_dinero(request).filter(id_ministerio__nombre_ministerio__icontains=query)
        return render(request, 'movimiento_dinero/buscar_movimiento_dinero.html', {'movimientos': movimientos})
    movimientos =  get_queryset_movimiento_dinero(request)
    return render(request, 'movimiento_dinero/buscar_movimiento_dinero.html', {'movimientos': movimientos})

@login_required
def buscar_por_evento(request):
    query = request.GET.get('q')
    if query is not None and query != ' ' and request.is_ajax():
        movimientos = get_queryset_movimiento_dinero(request).filter(id_evento__descripcion_evento__icontains=query)
        return render(request, 'movimiento_dinero/buscar_movimiento_dinero.html', {'movimientos': movimientos})
    movimientos =  get_queryset_movimiento_dinero(request)
    return render(request, 'movimiento_dinero/buscar_movimiento_dinero.html', {'movimientos': movimientos})

@login_required
def buscar_por_id(request):
    query = request.GET.get('q')
    if query is not None and query != ' ' and request.is_ajax():
        movimientos = get_queryset_movimiento_id(request).filter(id__icontains=query)
        return render(request, 'movimiento_dinero/buscar_movimiento_dinero.html', {'movimientos': movimientos})
    movimientos =  get_queryset_movimiento_id(request)
    return render(request, 'movimiento_dinero/buscar_movimiento_dinero.html', {'movimientos': movimientos})
"""
#def movimiento_restore(request, pk):
#    movimiento = Movimiento_Dinero.objects.get(id=pk)
#    if request.method == 'GET':
#        movimiento.fecha_baja_movimiento_dinero = None
#        movimiento.save()
#        return redirect('movimiento:movimiento_listar')
#    return redirect('movimiento:movimiento_list_baja')
#
vcaja = 0
def caja(request):
	global vcaja

	movimiento = Movimiento_Dinero.objects.all()

	for mov in movimiento:
		if mov.tipo_movimiento == 'Ingreso':
			vcaja += mov.monto_movimiento
		else:
			vcaja -= mov.monto_movimiento
		return vcaja

	contexto = {'movimientos':movimiento,
	}

	return render(request, 'movimiento_dinero/movimiento_dinero_list.html', contexto, vcaja)

@login_required
def reporte_movimiento(request):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="MovimientosDeDinero.pdf"'
    r = ReporteMovimientoCajaPDF(request)
    response.write(r.run())
    return response

class ReporteMovimientoCajaPDF(View):
    def __init__(self,get_dict):
        self.buf = BytesIO()
        self.get_dict = get_dict

    def run(self):
        self.doc = SimpleDocTemplate(self.buf)
        self.story = []
        self.encabezado()
        self.crearTabla()
        self.doc.build(self.story, onFirstPage=self.numeroPagina,
        		onLaterPages=self.numeroPagina)
        pdf = self.buf.getvalue()
        self.buf.close()
        return pdf

    def encabezado(self):
                #Utilizamos el archivo logo_django.png que está guardado en la carpeta media/imagenes
                archivo_imagen = settings.MEDIA_ROOT+'/imagenes/ijme.negro.png'
                #Definimos el tamaño de la imagen a cargar y las coordenadas correspondientes
                im = Image(archivo_imagen, 120, 120)
                self.story.append(im)
                espacio = Spacer(width=20, height=20)
                self.story.append(espacio)
                p = Paragraph("Reporte de Movimientos de Dinero", self.estiloPC())
                self.story.append(p)
                self.story.append(Spacer(1,0.5*inch))

    def crearTabla(self):
        movimientos = buscar(self.get_dict)
        print(movimientos)
        data = [["N°","Fecha","Clase","Categoria","Ingreso","Egreso","Autor"]] \
            +[[x.id ,x.fecha_movimiento, x.clase_movimiento, x.categoria, x.ingreso, x.egreso, x.id_persona]

        #for x in Movimiento_Dinero.objects.filter(fecha_baja_movimiento_dinero__isnull=True)]
           for x in movimientos]

        style = TableStyle([
           ('GRID', (0,0), (-1,-1), 0.25, colors.black),
           ('ALIGN',(0,0),(-1,-1),'CENTER'),
           ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
           ('FONTSIZE', (0, 0), (-1, -1), 10)])

        t = Table(data)
        t.setStyle(style)
        self.story.append(t)

    def estiloPC(self):
    	      return ParagraphStyle(name="centrado", alignment=TA_CENTER, fontSize = 20)

    def numeroPagina(self,canvas,doc):
    	      num = canvas.getPageNumber()
    	      text = "Pagina %s" % num
    	      canvas.drawRightString(200*mm, 20*mm, text)

class ReporteMovimientoCajaExcel(TemplateView):

        #Usamos el método get para generar el archivo excel
        def get(self, request, *args, **kwargs):
                #Obtenemos todas las personas de nuestra base de datos
                movimientos = Movimiento_Dinero.objects.filter(fecha_baja_movimiento_dinero__isnull=True)
                """movimientos = buscar(self.get_dict)"""
                #Creamos el libro de trabajo
                wb = Workbook()
                #Definimos como nuestra hoja de trabajo, la hoja activa, por defecto la primera del libro
                ws = wb.active
                #En la celda B1 ponemos el texto 'REPORTE DE PERSONAS'
                ws['B1'] = 'REPORTE DE MOVIMIENTOS DE DINERO'
                #Juntamos las celdas desde la B1 hasta la E1, formando una sola celda
                ws.merge_cells('B1:K1')
                ws.merge_cells('H2:I2')
                ws.merge_cells('J2:K2')
                #Creamos los encabezados desde la celda B3 hasta la E3
                ws['B3'] = 'Nª'
                ws['C3'] = 'Fecha Movimiento'
                ws['D3'] = 'Fijo/Eventual'
                ws['E3'] = 'Categoría'
                ws['F3'] = 'Ingreso'
                ws['G3'] = 'Egreso'
                ws['H2'] = 'Autor'
                ws['H3'] = 'Nombre'
                ws['I3'] = 'Apellido'
                ws['J3'] = 'Descripcion'
                #ws['H2'] = 'Evento'
                #ws['K3'] = 'Descripcion Evento'
                #ws['L3'] = 'Fecha del Evento'
                #ws['M3'] = 'Ministerio'
                cont=4
                #ws.cell(['B1']).border = Border(top = Side(border_style='thin', color='FF000000'), right = Side(border_style='thin', color='FF000000'), bottom = Side(border_style='thin', color='FF000000'), left = Side(border_style='thin', color='FF000000'))
                #Recorremos el conjunto de personas y vamos escribiendo cada uno de los datos en las celdas
                for movimiento in movimientos:
                        ws.cell(row=cont,column=2).value = movimiento.fecha_movimiento
                        ws.cell(row=cont,column=3).value = movimiento.clase_movimiento
                        ws.cell(row=cont,column=5).value = movimiento.categoria
                        ws.cell(row=cont,column=6).value = movimiento.ingreso
                        ws.cell(row=cont,column=7).value = movimiento.egreso
                        ws.cell(row=cont,column=8).value = movimiento.id_persona.nombre
                        ws.cell(row=cont,column=9).value = movimiento.id_persona.apellido_persona
                        ws.cell(row=cont,column=10).value = movimiento.descripcion_movimiento                        #ws.cell(row=cont,column=10).value = movimiento.id_evento
                        #ws.cell(row=cont,column=11).value = movimiento.id_evento
                        #ws.cell(row=cont,column=12).value = movimiento.id_ministerio
                        cont = cont + 1
                #Establecemos el nombre del archivo
                nombre_archivo ="ReporteMovimientoDineroExcel.xlsx"
                #Definimos que el tipo de respuesta a devolver es un archivo de microsoft excel
                response = HttpResponse(content_type="application/ms-excel")
                contenido = "attachment; filename={0}".format(nombre_archivo)
                response["Content-Disposition"] = contenido
                wb.save(response)
                return response

#Categorias
"""
def categoria_view(request):
    if request.method == 'POST':
        form = CategoriaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('categoria:categoria_listar')
    else:
        form = CategoriaForm
    return render(request, 'categoria/categoria_form.html', {'form':form})


def categoria_list(request):
	categoria = Categoria.objects.all().order_by('id')
	contexto = {'categoria':categoria}
	return render(request, 'categoria/categoria_list.html', contexto)

def categoria_edit(request, id_categoria):
    categoria = Categoria.objects.get(id=id_categoria)
    if request.method == 'GET':
        form = CategoriaForm(instance=categoria)
    else:
        form = CategoriaForm(request.POST, instance=categoria)
        if form.is_valid():
            form.save()
            return redirect('categoria:categoria_listar')
    return render(request,'categoria/categoria_form.html',{'form':form})

def categoria_delete(request, id_categoria):
    categoria = Categoria.objects.get(id=id_categoria)
    if request.method == 'POST':
        categoria.delete()
        return redirect('categoria:categoria_listar')
    return render(request,'categoria/categoria_delete.html',{'categoria':categoria})




class CategoriaList(ListView):
	model = Categoria
	template_name = 'categoria/categoria_list.html'
	paginate_by = 3

class CategoriaCreate(CreateView):
	model = Categoria
	form_class = CategoriaForm
	template_name = 'categoria/categoria_form.html'
	success_url = reverse_lazy('categoria:categoria_listar')

class CategoriaUpdate(UpdateView):
	model = Categoria
	form_class = CategoriaForm
	template_name = 'categoria/categoria_form.html'
	success_url = reverse_lazy('categoria:categoria_listar')


class CategoriaDelete(DeleteView):
	model = Categoria
	template_name = 'categoria/categoria_delete.html'
	success_url = reverse_lazy('categoria:categoria_listar')
"""
#class BuscarView(TemplateView):
#	def post(self, request, *args, **kwargs):
#		buscar = request.POST['buscalo']
#		movi1 = Movimiento_Dinero.objects.filter(fecha_movimiento__icontains=buscar)
#		movi2 = Movimiento_Dinero.objects.filter(clase_movimiento__icontains=buscar)
#		movi3 = Movimiento_Dinero.objects.filter(tipo_movimiento__icontains=buscar)
#		movi4 = Movimiento_Dinero.objects.filter(categoria__icontains=buscar)
#		movi5 = Movimiento_Dinero.objects.filter(monto_movimiento__icontains=buscar)
#		movi6 = Movimiento_Dinero.objects.filter(descripcion_movimiento__icontains=buscar)
#		movi7 = Movimiento_Dinero.objects.filter(id_persona__nombre__icontains=buscar)
#		movi8 = Movimiento_Dinero.objects.filter(id_evento__descripcion_evento__icontains=buscar)
	#	movi9 = Movimiento_Dinero.objects.filter(id_ministerio__nombre_ministerio__icontains=buscar)
	#	contexto = {
	#	'movimientos1':movi1,
	#	'movimientos2':movi2,
	#	'movimientos3':movi3,
		#'movimientos4':movi4,
	#	'movimientos5':movi5,
		#'movimientos6':movi6,
		#'movimientos7':movi7,
	#	'movimientos8':movi8,
		#'movimientos9':movi9,
		#}
		#return render(request,'movimiento_dinero/buscar.html', contexto)
