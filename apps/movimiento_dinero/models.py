from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from apps.persona.models import Persona
from apps.evento.models import Evento
#from apps.persona.models import Matrimonio
from apps.ministerio.models import Ministerios


tipo_movimiento_choices = (
    ('Ingreso', 'Ingreso'),
    ('Egreso', 'Egreso'),
)

detalle_categoria_choices = (
        ('Ingreso', (
            ('Diezmo', 'Diezmo'),
            ('Donación', 'Donación'),
            ('Ofrenda', 'Ofrenda'),
            ('Venta', 'Venta'),
          )
         ),
         ('Egreso', (
            ('Donación', 'Donación'),
            ('Gastos especiales', 'Gastos especiales'),
            ('Gastos generales', 'Gastos generales'),
            ('Gastos ministeriales', 'Gastos ministeriales'),
            ('Ofrenda', 'Ofrenda'),
            ('Venta', 'Venta'),
          )
         )
)

clase_movimiento_choices = (
    ('Fijo', 'Fijo'),
    ('Eventual', 'Eventual'),
)

# Create your models here.
"""class Categoria(models.Model):
    tipo_movimiento = models.CharField(max_length=50, choices = tipo_movimiento_choices,null=False, blank=False) ##I/E
    nombre_categoria = models.CharField(max_length=50)
    def __str__(self):
        return self.nombre_categoria
"""
"""
class MovimientoQuerySet(models.Queryset):
    def search(self, query=None):
        qs = self
        if query is not None:
            or_lookup = (Q(id_persona__nombre__icontains=query)|
                         Q(id_ministerio__nombre_ministerio__icontains=query)|
                         Q(id_movimiento_dinero__icontains=query)|
                         Q(fecha_movimiento__gte=query)|
                         Q(fecha_movimiento__lte=query))
            qs = qs.filter(or_lookup).distinct()
        return qs

class MovimientoManager(models.Manager):
    def get_queryset(self):
        return PosQuerySet(self.model, using=self._db)

    def search(self, query=None):
        return self.get_queryset().search(query=query)
"""

class Movimiento_Dinero(models.Model):
    fecha_movimiento = models.DateField(auto_now_add = False, null=False, blank=False)
    #fecha_movimiento = models.CharField(max_length=10,null=False, blank=False)
    clase_movimiento = models.CharField(max_length=10,null=False, blank=False, choices = clase_movimiento_choices)  ##F/E
    categoria = models.CharField(max_length=50, null=False, blank=False, choices = detalle_categoria_choices)
    ingreso = models.DecimalField(max_digits=8, decimal_places=2, default=0.00,validators=[MinValueValidator(0)])
    egreso = models.DecimalField(max_digits=8, decimal_places=2, default=0.00,validators=[MinValueValidator(0)])
    descripcion_movimiento = models.CharField(max_length=60, null=False, blank=False)
    id_persona = models.ForeignKey(Persona, null=False, blank=False, on_delete=models.CASCADE)
    id_evento = models.ForeignKey(Evento, null=True, blank=True, on_delete=models.CASCADE)
    #id_matrimonio = models.ForeignKey(Matrimonio, null=True, blank=True, on_delete=models.CASCADE)
    id_ministerio = models.ForeignKey(Ministerios, null=True, blank=True, on_delete=models.CASCADE)
    fecha_baja_movimiento_dinero = models.DateField(max_length=50, blank=True, null=True)
    comentario = models.CharField(max_length=50, null=True, blank=True)


    class Meta:
                permissions = (('ver_detalle_movimiento_dinero', 'Ver detalle movimientos de dinero'),)

    def __str__(self):
        return self.descripcion_movimiento

    def clean(self):
            if self.ingreso and self.egreso:
                raise ValidationError('Error: No se pueden registrar ingresos y egresos juntos')
            if self.ingreso == 0 and self.egreso == 0:
                raise ValidationError('Error: Ingrese la cantidad de dinero')
