from django.urls import include, path, re_path
from apps.movimiento_dinero import views
from django.contrib.auth.views import login_required
from apps.movimiento_dinero.views import movimiento_list_baja, movimiento_view, movimiento_list, movimiento_delete, \
reporte_movimiento, ReporteMovimientoCajaExcel#, buscar_autor_movimiento_dinero, buscar_por_tipo_movimiento, buscar_por_clase_movimiento, buscar_por_ministerio,\
#buscar_por_id, buscar_por_evento #CategoriaList, CategoriaCreate, CategoriaUpdate, CategoriaDelete, categoria_view, movimiento_restore, movimiento_edit, categoria_list, categoria_edit, categoria_delete,caja, BuscarView,

urlpatterns = [
    re_path(r'^movimiento/listar$', views.movimiento_list , name='movimiento_listar'),
    re_path(r'^movimiento/buscar$', views.movimiento_buscar , name='movimiento_buscar'),
    #url(r'^movimiento/listar$', views.caja , name='caja'),
	re_path(r'^movimiento/listar/baja/$', views.movimiento_list_baja , name='movimiento_listar_baja'),
    re_path(r'^movimiento/nueva$', views.movimiento_view , name='movimiento_crear'),
    #url(r'^movimiento/editar/(?P<pk>\d+)/$', views.movimiento_edit , name='movimiento_editar'),
    re_path(r'^movimiento/eliminar/(?P<pk>\d+)/$', views.movimiento_delete , name='movimiento_eliminar'),
    #url(r'^movimiento/buscarautormovimiento/$', views.buscar_autor_movimiento_dinero, name='buscarautormovimiento'),
    #url(r'^movimiento/buscarportipomovimiento/$', views.buscar_por_tipo_movimiento, name='buscarportipomovimiento'),
    #url(r'^movimiento/buscarporclasemovimiento/$', views.buscar_por_clase_movimiento, name='buscarporclasemovimiento'),
    #url(r'^movimiento/buscarporministerio/$', views.buscar_por_ministerio, name='buscarporministerio'),
    #url(r'^movimiento/buscarporevento/$', views.buscar_por_evento, name='buscarporevento'),
    #url(r'^movimiento/buscarporid/$', views.buscar_por_id, name='buscarporid'),
	#url(r'^movimiento/restaurar/(?P<pk>\d+)/$', views.movimiento_restore , name='movimiento_restaurar'),
	#url(r'^nuevo_categoria$',login_required(CategoriaCreate.as_view()), name='categoria_crear'),
    #url(r'^listar_categoria$', login_required(CategoriaList.as_view()), name='categoria_listar'),
    #url(r'^editar_categoria/(?P<pk>\d+)/$', login_required(CategoriaUpdate.as_view()), name='categoria_editar'),
    #url(r'^eliminar_categoria/(?P<pk>\d+)/$', login_required(CategoriaDelete.as_view()), name='categoria_eliminar'),
    re_path(r'^reporte_caja_excel/$',views.ReporteMovimientoCajaExcel.as_view(), name="reporte_caja_excel"),
    re_path(r'^reporte_movimiento/$',views.reporte_movimiento, name="reporte_movimiento"),
    #url(r'^buscar/$', BuscarView.as_view(),name='buscar'),

]
