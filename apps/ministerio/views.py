from django.shortcuts import render, redirect

# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.urls import reverse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, TemplateView
import datetime
from apps.ministerio.forms import MinisteriosForm, MiembrosMinisterioForm
from apps.ministerio.models import Ministerios, MiembrosMinisterio
from django.db.models import Q
from django.contrib.auth.decorators import login_required


@login_required
def ministerio_view(request):
    if request.method == 'POST':
        form = MinisteriosForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('ministerio:ministerio_listar')
    else:
        form = MinisteriosForm
    return render(request, 'ministerio/ministerio_form.html', {'form':form})

@login_required
def ministerio_list(request):
	ministerio = Ministerios.objects.filter(fecha_baja_ministerio__isnull=True).order_by('nombre_ministerio')
	contexto = {'ministerios':ministerio}
	return render(request, 'ministerio/ministerio_listar.html', contexto)

@login_required
def get_queryset_ministerio(self):
    queryset = Ministerios.objects.filter(fecha_baja_ministerio__isnull=True).order_by('nombre_ministerio')#
    return queryset

@login_required
def ministerio_list_baja(request):
	ministerio = Ministerios.objects.filter(fecha_baja_ministerio__isnull=False).order_by('nombre_ministerio')
	contexto = {'ministerios':ministerio}
	return render(request, 'ministerio/ministerio_listar_baja.html', contexto)

@login_required
def ministerio_edit(request, pk):
    ministerio = Ministerios.objects.get(id=pk)
    if request.method == 'GET':
        form = MinisteriosForm(instance=ministerio)
    else:
        form = MinisteriosForm(request.POST, instance=ministerio)
        if form.is_valid():
            form.save()
            return redirect('ministerio:ministerio_listar')
    return render(request,'ministerio/ministerio_form.html',{'form':form})

@login_required
def ministerio_delete(request, pk):
    ministerio = Ministerios.objects.get(id=pk)
    if request.method == 'POST':
	    ministerio.fecha_baja_ministerio = datetime.date.today()
	    ministerio.save()
	    return redirect('ministerio:ministerio_listar')
    return render(request,'ministerio/ministerio_delete.html',{'ministerio':ministerio})

@login_required
def ministerio_restore(request, pk):
    ministerio = Ministerios.objects.get(id=pk)
    if request.method == 'GET':
        ministerio.fecha_baja_ministerio = None
        ministerio.save()
        return redirect('ministerio:ministerio_listar')
    return redirect('ministerio:ministerio_listar_baja')

@login_required
def buscar_ministerio_nombre(request):
    query = request.GET.get('q')
    if query is not None and query != ' ' and request.is_ajax():
        ministerios = get_queryset_ministerio(request).filter(nombre_ministerio__icontains=query)
        return render(request, 'ministerio/buscar_ministerio.html', {'ministerios': ministerios})
    ministerios =  get_queryset_ministerio(request)
    return render(request, 'ministerio/buscar_ministerio.html', {'ministerios': ministerios})

@login_required
def buscar_ministerio_encargado(request):
    query = request.GET.get('q')
    if query is not None and query != ' ' and request.is_ajax():
        ministerios = get_queryset_ministerio(request).filter(Q(encargado_ministerio__nombre__icontains=query)|Q(encargado_ministerio__apellido_persona__icontains=query))
        return render(request, 'ministerio/buscar_ministerio.html', {'ministerios': ministerios})
    ministerios =  get_queryset_ministerio(request)
    return render(request, 'ministerio/buscar_ministerio.html', {'ministerios': ministerios})

#MIEMBROS
@login_required
def ministerio_agregar_miembros(request):
	if request.method == 'POST':
		form = MiembrosMinisterioForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('ministerio:ministerio_agregar_miembros')
	else:
	    form = MiembrosMinisterioForm
	return render(request, 'ministerio/ministerio_agregar_miembros.html', {'form':form})

@login_required
def miembros_ministerio_list(request, pk):
	miembros_ministerio = MiembrosMinisterio.objects.filter(ministerio_id=pk, fecha_baja_miembro_ministerio__isnull=True)
	ministerio = Ministerios.objects.filter(id=pk)
	contexto = {'miembros_ministerio':miembros_ministerio,
				'ministerios':ministerio}
	return render(request, 'ministerio/miembros_ministerio_listar.html', contexto)

@login_required
def miembros_ministerio_list_baja(request, pk):
	miembro_ministerio = MiembrosMinisterio.objects.filter(ministerio_id=pk, fecha_baja_miembro_ministerio__isnull=False)
	contexto = {'miembros_ministerio':miembro_ministerio}
	return render(request, 'ministerio/miembros_ministerio_listar_baja.html', contexto)

@login_required
def miembros_ministerio_delete(request, pk):
    miembro_ministerio = MiembrosMinisterio.objects.get(id=pk)
    if request.method == 'POST':
	    miembro_ministerio.fecha_baja_miembro_ministerio = datetime.date.today()
	    miembro_ministerio.save()
	    return redirect('ministerio:miembros_ministerio_listar', miembro_ministerio.ministerio_id)
    return render(request,'ministerio/miembros_ministerio_delete.html',{'miembros_ministerio':miembro_ministerio})

@login_required
def miembros_ministerio_restore(request, pk):
    miembro_ministerio = MiembrosMinisterio.objects.get(id=pk)
    if request.method == 'GET':
        miembro_ministerio.fecha_baja_miembro_ministerio = None
        miembro_ministerio.save()
        return redirect('ministerio:miembros_ministerio_listar_baja', miembro_ministerio.ministerio_id)
    return redirect('ministerio:miembros_ministerio_listar', miembro_ministerio.ministerio_id)
#
#class BuscarView(TemplateView):
#	def post(self, request, *args, **kwargs):
#		buscar = request.POST['buscalo']
#		min1 = Ministerios.objects.filter(nombre_ministerio__icontains=buscar)
#		min2 = Ministerios.objects.filter(dia_actividad__icontains=buscar)
#		min3 = Ministerios.objects.filter(lugar_actividad__icontains=buscar)
#		min4 = Ministerios.objects.filter(hora_actividad__icontains=buscar)
#		min5 = Ministerios.objects.filter(encargado_ministerio__nombre__icontains=buscar)
		#ministerios5 = Ministerios.objects.filter(encargado_ministerio__nombre__icontains=buscar)
#		contexto = {
	#	'ministerios1': min1, #ministerios1, #'min1' : True,
#        'ministerios2': min2, #ministerios2, #'min2' : True,
    #    'ministerios3': min3, #ministerios3, #'min3' : True,
    #    'ministerios4': min4, #ministerios4, #'min4' : True,
    #    'ministerios5': min5 #ministerios5, #'min5' : True,
	#	}
	#	return render(request,'ministerio/buscar.html', contexto)
