from django import forms

from apps.ministerio.models import Ministerios, MiembrosMinisterio, Persona

class MinisteriosForm(forms.ModelForm):
    encargado_ministerio = forms.ModelChoiceField(queryset=Persona.objects.filter(fecha_baja_persona__isnull=True))

    def __init__(self, *args, **kwargs):
        super(MinisteriosForm, self).__init__(*args, **kwargs)
        self.fields['encargado_ministerio'].label = "Encargado"

    class Meta:
        model = Ministerios

        fields = [
            'nombre_ministerio',
            'dia_actividad',
            'lugar_actividad',
            'hora_actividad',
            'encargado_ministerio',
        ]
        labels = {
            'nombre_ministerio': 'Nombre:',
            'dia_actividad': 'Dia:',
            'lugar_actividad': 'Lugar:',
            'hora_actividad': 'Hora:',
            'encargado_ministerio': 'Encargado:',
        }

class MiembrosMinisterioForm(forms.ModelForm):

    class Meta:
        model = MiembrosMinisterio

        fields = [
            'ministerio',
            'persona',
            'fecha_ingreso',
            'comentario',
        ]
        labels = {
            'ministerio': 'Ministerio',
            'persona': 'Miembro',
            'fecha_ingreso': 'Fecha de ingreso',
            'comentario': 'Comentario',
        }
