from django.urls import include, path, re_path
from django.contrib.auth.views import login_required
from apps.ministerio import views
from apps.ministerio.views import ministerio_view, ministerio_list, ministerio_edit, \
ministerio_delete, ministerio_list_baja, ministerio_restore, ministerio_agregar_miembros, \
miembros_ministerio_list, miembros_ministerio_delete, miembros_ministerio_list_baja, miembros_ministerio_restore, \
buscar_ministerio_nombre, buscar_ministerio_encargado

urlpatterns = [
    re_path(r'^ministerio/nuevo$', views.ministerio_view , name='ministerio_crear'),
    re_path(r'^ministerio/listar$', views.ministerio_list , name='ministerio_listar'),
    re_path(r'^ministerio/editar/(?P<pk>\d+)$', views.ministerio_edit , name='ministerio_editar'),
    re_path(r'^ministerio/eliminar/(?P<pk>\d+)$', views.ministerio_delete , name='ministerio_eliminar'),
    re_path(r'^ministerio/listar/agregar/$', views.ministerio_agregar_miembros , name='ministerio_agregar_miembros'),
    re_path(r'^ministerio/listar/agregar/detalle/(?P<pk>\d+)$', views.miembros_ministerio_list , name='miembros_ministerio_listar'),
    re_path(r'^ministerio/listar/agregar/detalle/baja/listar/(?P<pk>\d+)$', views.miembros_ministerio_list_baja , name='miembros_ministerio_listar_baja'),
    re_path(r'^ministerio/listar/agregar/detalle/baja/(?P<pk>\d+)$', views.miembros_ministerio_delete , name='miembros_ministerio_eliminar'),
    re_path(r'^ministerio/listar/agregar/detalle/baja/restaurar/(?P<pk>\d+)$', views.miembros_ministerio_restore , name='miembros_ministerio_restaurar'),
    re_path(r'^ministerio/listar/baja/$', views.ministerio_list_baja , name='ministerio_listar_baja'),
    re_path(r'^ministerio/restaurar/(?P<pk>\d+)/$', views.ministerio_restore , name='ministerio_restaurar'),
    re_path(r'^ministerio/buscarministerio/$', views.buscar_ministerio_nombre, name='buscarministerio'),
    re_path(r'^ministerio/buscarpersonaministerio/$', views.buscar_ministerio_encargado, name='buscarpersonaministerio')
]
