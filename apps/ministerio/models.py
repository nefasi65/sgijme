from django.db import models

# Create your models here.
from apps.persona.models import Persona

dias_semana_choices = (
    ('Lunes', 'Lunes'),
    ('Martes', 'Martes'),
    ('Miércoles', 'Miércoles'),
    ('Jueves', 'Jueves'),
    ('Viernes', 'Viernes'),
    ('Sábado', 'Sábado'),
    ('Domingo', 'Domingo'),
)

class Ministerios(models.Model):
    nombre_ministerio = models.CharField(max_length=50, blank=False)
    dia_actividad = models.CharField(max_length=10, choices = dias_semana_choices, blank=False)
    lugar_actividad = models.CharField(max_length=30, blank=False)
    hora_actividad = models.TimeField(null=True, blank=False)
    encargado_ministerio = models.ForeignKey(Persona, null=False, blank=False, related_name = 'encargado_ministerio', on_delete=models.CASCADE)
    miembros_ministerio = models.ManyToManyField(Persona, through='MiembrosMinisterio')
    fecha_baja_ministerio = models.DateField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.nombre_ministerio

    class Meta:
                permissions = (('ver_detalle_ministerio', 'Ver detalle ministerios'),)
                unique_together = (('dia_actividad','hora_actividad','encargado_ministerio',),('dia_actividad','hora_actividad','lugar_actividad',))

    def unique_error_message(self, model_class, unique_check):
        error = super().unique_error_message(model_class, unique_check)
        # Intercept the unique_together error
        if unique_check:
            error.message = 'Encargado o dirección no disponible en este día y horario.'
            return error

class MiembrosMinisterio(models.Model):
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    ministerio = models.ForeignKey(Ministerios, on_delete=models.CASCADE)
    fecha_ingreso = models.CharField(max_length=30)
    comentario = models.CharField(max_length=64, blank=True, null=True)
    fecha_baja_miembro_ministerio = models.DateField(max_length=10, blank=True, null=True)

    def __str__(self):
        return self.persona

    class Meta:
                permissions = (('ver_detalle_miembros_ministerio', 'Ver detalle miembros de ministerio'),)
                unique_together = ("persona","ministerio"),
