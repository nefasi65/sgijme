from django.urls import include, path, re_path
from apps.portada import views

urlpatterns = [

    re_path(r'^index$',views.video, name='post_list'), #views.post_list
    re_path('post/(?P<pk>\d+)/$', views.post_detail, name='post_detail'),
    re_path('post/new/', views.post_new, name='post_new'),
    re_path('post/post_new_borrador/', views.post_new_borrador, name='post_new_borrador'),
    re_path('post/editar/(?P<pk>\d+)/$', views.post_edit, name='post_edit'),
    re_path('post/borrador/$', views.post_draft_list, name='post_draft_list'),
    re_path(r'^post/(?P<pk>\d+)/publicar/$', views.post_publish, name='post_publish'),
    re_path(r'^post/(?P<pk>\d+)/borrar/$', views.post_remove, name='post_remove'),


]
