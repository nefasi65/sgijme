from django.shortcuts import render
from django.utils import timezone
from .models import Item, Post
from django.shortcuts import render, get_object_or_404
#from .forms import PostForm
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required


def video(request):
    obj = Item.objects.all()
    return render(request, 'portada/post_list.html', {'obj': obj})

#@login_required
def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')
    return render(request, 'portada/post_list.html', {'posts': posts})

@login_required
def post_detail(request, pk):
    post = get_object_or_404(Post, id=pk)
    return render(request, 'portada/post_detail.html', {'post': post})

@login_required
def post_new_borrador(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('portada.views.post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'portada/post_edit_borrador.html', {'form': form})

@login_required
def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('portada.views.post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'portada/post_edit.html', {'form': form})

@login_required
def post_edit(request, pk):
    post = get_object_or_404(Post, id=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('portada:post_detail', pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'portada/post_edit.html', {'form': form})

@login_required
def post_draft_list(request):
    posts = Post.objects.filter(published_date__isnull=True).order_by('created_date')
    return render(request, 'portada/post_draft_list.html', {'posts': posts})

@login_required
def post_publish(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.publish()
    return redirect('portada:post_list')#'portada:post_detail', pk=pk)

@login_required
def post_remove(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.delete()
    return redirect('portada:post_list')
