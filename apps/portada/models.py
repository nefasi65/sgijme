from django.db import models
from django.utils import timezone
#from ckeditor_uploader.fields import RichTextUploadingField
from django.urls import reverse
from embed_video.fields import EmbedVideoField

class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
#    text = RichTextUploadingField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title


class Item(models.Model):
    video = EmbedVideoField()  # same like models.URLField()
