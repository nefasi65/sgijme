from django.db import models
from django.db.models import signals
from django.core.exceptions import ValidationError
from apps.ministerio.models import Ministerios
from apps.inventario.models import Articulo



class Salida(models.Model):
    id_articulo = models.ForeignKey(Articulo, null=True, blank=True, on_delete=models.CASCADE)
    id_ministerio = models.ForeignKey(Ministerios, null=True, blank=True, on_delete=models.CASCADE)
    cantidad_salida = models.PositiveSmallIntegerField()
    fecha_salida = models.CharField(max_length=100, null=False, blank=False)
    observaciones_salida = models.CharField(max_length=100)
    fecha_baja_salida = models.DateField(max_length=50, blank=True, null=True)

    def update_stock(id_articulo,cantidad,baja):
        if(baja == True):
            id_articulo.cantidad_articulo += cantidad
        elif(baja == False):
            id_articulo.cantidad_articulo -= cantidad
        id_articulo.save()

    class Meta:
                permissions = (('ver_detalle_salida', 'Ver detalle miembros de salidas'),)

    def clean(self):
            if self.cantidad_salida > self.id_articulo.cantidad_articulo:
                raise ValidationError('Error: Las salidas no pueden superar el monto en stock.')
