from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.urls import reverse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
import datetime
from apps.salida.models import Salida
from apps.salida.forms import SalidaForm
from apps.inventario.models import Articulo
from django.contrib.auth.decorators import login_required
from apps.persona.models import Persona



@login_required
def salida_view(request):
    if request.method == 'POST':
        form = SalidaForm(request.POST)
        if form.is_valid():
            id_articulo = form.cleaned_data['id_articulo']
            cantidad_salida = form.cleaned_data['cantidad_salida']
            Salida.update_stock(id_articulo,cantidad_salida,False)
            form.save()
            return redirect('entradasalida:entradasalida_listar')
    else:
        form = SalidaForm
    return render(request, 'salida/salida_form.html', {'form':form})

@login_required
def salida_list(request):
	salida = Salida.objects.filter(fecha_baja_salida__isnull=True)
	contexto = {'salidas':salida}
	return render(request, 'entradasalida/entradasalida_listar.html', contexto)

@login_required
def validar_cantidad_salida(request):
	id_articulo = request.GET.get('id_articulo')
	if query is not None and query != ' ' and request.is_ajax():
		if(id_articulo):
			articulo = Articulo.objects.filter(pk=id_articulo)
			cantidad_articulo = articulo.cantidad_articulo
		else:
			cantidad_articulo = "Debe seleccionar un articulo"
		data = {
        	'cantidad': cantidad_articulo
    	}
	return render(Jsonresponse(data))

#def salida_edit(request, id_salida):
#    salida = Salida.objects.get(id=id_salida)
#    if request.method == 'GET':
#        form = SalidaForm(instance=salida)
#    else:
#        form = SalidaForm(request.POST, instance=salida)
#        if form.is_valid():
#            form.save()
#            return redirect('salida:salida_listar')
#    return render(request,'salida/salida_form.html',{'form':form})
@login_required
def salida_delete(request, pk):
    salida = Salida.objects.get(id=pk)
    if request.method == 'POST':
        salida.fecha_baja_salida = datetime.date.today()
        Salida.update_stock(salida.id_articulo,salida.cantidad_salida,True)
        salida.save()
        return redirect('entradasalida:entradasalida_listar')
    return render(request,'salida/salida_delete.html',{'salida':salida})
