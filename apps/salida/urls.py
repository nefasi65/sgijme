from django.urls import path, re_path
from django.contrib.auth.views import login_required
from apps.salida import views
from apps.salida.views import salida_view, salida_list, salida_delete
urlpatterns = [
    re_path(r'^salida/nueva$', views.salida_view , name='salida_crear'),
    re_path(r'^salida/listar$', views.salida_list , name='salida_listar'),
    re_path(r'^salida/eliminar/(?P<pk>\d+)$', views.salida_delete , name='salida_eliminar'),
    re_path(r'^ajax/validar_cantidad_salida/$', views.validar_cantidad_salida, name='validar_cantidad_salida'),


]
