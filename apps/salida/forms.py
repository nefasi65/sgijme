from django import forms

from apps.salida.models import Salida, Articulo, Ministerios

class SalidaForm(forms.ModelForm):
    id_articulo = forms.ModelChoiceField(queryset=Articulo.objects.filter(fecha_baja_articulo__isnull=True))
    id_ministerio = forms.ModelChoiceField(queryset=Ministerios.objects.filter(fecha_baja_ministerio__isnull=True))

    def __init__(self, *args, **kwargs):
        super(SalidaForm, self).__init__(*args, **kwargs)
        self.fields['id_articulo'].label = "Inventario"
        self.fields['id_ministerio'].label = "Ministerio"
    class Meta:
        model = Salida

        fields = [
            'id_articulo',
            'id_ministerio',
            'cantidad_salida',
            'fecha_salida',
            'observaciones_salida',
        ]
        labels = {
            'id_articulo': 'Inventario',
            'id_ministerio': 'Ministerio',
            'cantidad_salida': 'Cantidad de Salidas',
            'fecha_salida': 'Fecha de Salidas',
            'observaciones_salida': 'Observaciones',
        }
