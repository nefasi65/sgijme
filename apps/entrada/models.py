from django.db import models
from django.db.models import signals
from django.core.exceptions import ValidationError
from apps.ministerio.models import Ministerios
from apps.inventario.models import Articulo



class Entrada(models.Model):
    id_articulo = models.ForeignKey(Articulo, null=True, blank=True, on_delete=models.CASCADE)
    id_ministerio = models.ForeignKey(Ministerios, null=True, blank=True, on_delete=models.CASCADE)
    cantidad_entrada = models.PositiveSmallIntegerField()
    fecha_entrada = models.CharField(max_length=100,null=False, blank=False)
    observaciones_entrada = models.CharField(max_length=100)
    fecha_baja_entrada = models.DateField(max_length=50, blank=True, null=True)

    def __str__(self):
        return '{} {}'.format(self.id_articulo,self.observaciones_entrada)

    def update_stock(id_articulo,cantidad,baja):
        if(baja == True):
            id_articulo.cantidad_articulo -= cantidad
        elif(baja == False):
            id_articulo.cantidad_articulo += cantidad
        id_articulo.save()

    class Meta:
                permissions = (('ver_detalle_entrada', 'Ver detalle entradas'),)

    #def clean(self):
    #        if self.cantidad_entrada > self.id_articulo.cantidad_articulo:
    #            raise ValidationError('Error: Las entradas no pueden sueprar el monto en stock.')


# register the signal
#signals.post_save.connect(update_stock, sender=Entrada, dispatch_uid="update_stock_count")




def suma(self):
		return self.cantidad_entrada * self.id_articulo.precio_Compra_articulo
