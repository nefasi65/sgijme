from django.urls import path, re_path
from django.contrib.auth.views import login_required
from apps.entrada import views
from apps.entrada.views import entrada_view, entrada_list, entrada_delete
urlpatterns = [
    re_path(r'^entrada/nueva$', views.entrada_view , name='entrada_crear'),
    re_path(r'^entrada/listar$', views.entrada_list , name='entrada_listar'),
    #url(r'^entrada/editar/(?P<pk>\d+)$', views.entrada_edit , name='entrada_editar'),
    re_path(r'^entrada/eliminar/(?P<pk>\d+)$', views.entrada_delete , name='entrada_eliminar'),
	#url(r'^entrada/listar/baja/$', views.entrada_list_baja , name='entrada_listar_baja'),
	#url(r'^entrada/restaurar/(?P<pk>\d+)/$', views.entrada_restore , name='entrada_restaurar'),

]
