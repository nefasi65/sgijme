from django import forms

from apps.entrada.models import Entrada, Articulo, Ministerios

class EntradaForm(forms.ModelForm):
    id_articulo = forms.ModelChoiceField(queryset=Articulo.objects.filter(fecha_baja_articulo__isnull=True))
    id_ministerio = forms.ModelChoiceField(queryset=Ministerios.objects.filter(fecha_baja_ministerio__isnull=True))

    def __init__(self, *args, **kwargs):
        super(EntradaForm, self).__init__(*args, **kwargs)
        self.fields['id_articulo'].label = "Inventario"
        self.fields['id_ministerio'].label = "Ministerio"
    class Meta:
        model = Entrada

        fields = [
            'id_articulo',
            'id_ministerio',
            'cantidad_entrada',
            'fecha_entrada',
            'observaciones_entrada',
        ]
        labels = {
            'id_articulo': 'Inventario',
            'id_ministerio': 'Ministerio',
            'cantidad_entrada': 'Cantidad de Entradas',
            'fecha_entrada': 'Fecha de Entrada',
            'observaciones_entrada': 'Observaciones',
        }
