from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.urls import reverse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
import datetime
from apps.entrada.models import Entrada
from apps.entrada.forms import EntradaForm
from django.contrib.auth.decorators import login_required
from apps.persona.models import Persona
from apps.inventario.models import Articulo


@login_required
def entrada_view(request):
    if request.method == 'POST':
        form = EntradaForm(request.POST)
        if form.is_valid():
            id_articulo = form.cleaned_data['id_articulo']
            cantidad_entrada = form.cleaned_data['cantidad_entrada']
            Entrada.update_stock(id_articulo,cantidad_entrada,False)
            form.save()
            return redirect('entradasalida:entradasalida_listar')
    else:
        form = EntradaForm
    return render(request, 'entrada/entrada_form.html', {'form':form})

@login_required
def entrada_list(request):
	entrada = Entrada.objects.filter(fecha_baja_entrada__isnull=True)
	contexto = {'entradas':entrada}
	return render(request, 'entradasalida/entradasalida_listar.html', contexto)

#def entrada_list_baja(request):
#	entrada = Entrada.objects.filter(fecha_baja_entrada__isnull=False)
#	contexto = {'entradas':entrada}
#	return render(request, 'entrada/entrada_listar_baja.html', contexto)

#def entrada_edit(request, pk):
#    entrada = Entrada.objects.get(id=pk)
#    if request.method == 'GET':
#        form = EntradaForm(instance=entrada)
#    else:
#        form = EntradaForm(request.POST, instance=entrada)
#        if form.is_valid():
#            form.save()
#            return redirect('entrada:entrada_listar')
#    return render(request,'entrada/entrada_form.html',{'form':form})
@login_required
def entrada_delete(request, pk):
    entrada = Entrada.objects.get(id=pk)
    if request.method == 'POST':
        entrada.fecha_baja_entrada = datetime.date.today()
        Entrada.update_stock(entrada.id_articulo,entrada.cantidad_entrada,True)
        entrada.save()
        return redirect('entradasalida:entradasalida_listar')
    return render(request,'entrada/entrada_delete.html',{'entrada':entrada})

#def entrada_restore(request, pk):
#    entrada = Entrada.objects.get(id=pk)
#    if request.method == 'GET':
#        entrada.fecha_baja_entrada = None
#		Entrada.update_stock(entrada.id_articulo,entrada.cantidad_entrada,False)
#        entrada.save()
#        return redirect('entrada:entrada_listar')
#    return redirect('entrada:entrada_listar_baja')
